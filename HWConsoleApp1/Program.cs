﻿using System;
using System.Management.Instrumentation;

namespace HWConsoleApp1
{
    
    class A { }

    class Program
    {
        static void Main(string[] args)
        {
            checked
            {
                byte a = 200;

                a += 100;

                Console.WriteLine(a);
            }
            Console.ReadKey();
            
        }


        public static int ProgramChoice()
        {
            Console.Clear();
            string programList = "From the following programs list, pick a program index: \n" +
                "0 - Compare two integers\n" +
                "1 - Integers falls between 5 and 10\n" +
                "2 - Integer 5 or 10\n" +
                "3 - Colorize the text in console\n" +
                "Enter -1 if you want to exit\n";
            Console.WriteLine(programList);

            
            int selectedProgram;
            Console.WriteLine("Number of program:");
            string userInput = Console.ReadLine();

            // use int instead of int
            bool success = int.TryParse(userInput, out selectedProgram);
            if (!success)
            {
                Console.Clear();   
                Console.WriteLine("Please enter an integer!");
                Console.WriteLine("\tPress any key to continue");
                Console.ReadKey();
                return -2;
            }
            else if ((selectedProgram < -1) || (selectedProgram > 3))
            {
                Console.Clear();
                Console.WriteLine("Please enter an index of program!");
                Console.WriteLine("\tPress any key to continue");
                Console.ReadKey();
            }
            Console.Clear();
            return selectedProgram;
        }

        static void Program0()
        {
            // Do not use type of variable in the name
            int typedInput1;
            Console.WriteLine("\nPlease enter your first number:");
            string userInput = Console.ReadLine();
            if (!int.TryParse(userInput, out typedInput1))
            {
                while (!int.TryParse(userInput, out typedInput1))
                {
                    Console.WriteLine("You entry is wrong, please enter an integer");
                    Console.WriteLine("Please enter your first number");
                    userInput = Console.ReadLine();
                }
            }

            int typedInput2;
            Console.WriteLine("Please enter your second number:");
            userInput = Console.ReadLine();
            if (!int.TryParse(userInput, out typedInput2))
            {
                while (!int.TryParse(userInput, out typedInput2))
                {
                    Console.WriteLine("You entry is wrong, please enter an integer");
                    Console.WriteLine("Please enter your first number");
                    userInput = Console.ReadLine();
                }
            }

            int result = (typedInput1 > typedInput2) ? 2 : (typedInput1 < typedInput2) ? 0 : 1;

            switch (result)
            {
                case 0:
                    Console.WriteLine("First number is less than second number");
                    break;
                case 1:
                    Console.WriteLine("First number is equal to second number");
                    break;
                case 2:
                    Console.WriteLine("First number is bigger than second number");
                    break;
            }
        }

        static void Program1()
        {
            int typedInput;
            Console.WriteLine("\nPlease enter your number:");
            string userInput = Console.ReadLine();
            if (!int.TryParse(userInput, out typedInput))
            {
                while (!int.TryParse(userInput, out typedInput))
                {
                    Console.WriteLine("You entry is wrong, please enter an integer");
                    Console.WriteLine("Please enter your number");
                    userInput = Console.ReadLine();
                }
            }

            string result = (typedInput > 5) && (typedInput < 10) ? "Number is between 5 and 10" : "unknown number";
            Console.WriteLine(result);
        }

        static void Program2()
        {
            int typedInput;
            Console.WriteLine("\nPlease enter your number:");
            string userInput = Console.ReadLine();
                while (!int.TryParse(userInput, out typedInput))
                {
                    Console.WriteLine("You entry is wrong, please enter an integer");
                    Console.WriteLine("Please enter your number");
                    userInput = Console.ReadLine();
                }

            string result = (typedInput == 5) || (typedInput == 10) ? "Number is either 5 or 10" : "unknown number";
            Console.WriteLine(result);
        }

        static void Program3()
        {
            Console.WriteLine("Select a color you wish:");
            foreach (ConsoleColor color in Enum.GetValues(typeof(ConsoleColor)))
            {
                Console.WriteLine(color);
            }

            string input;
            string formattedInput;
            bool notFound = true;
            while (notFound)
            {
                Console.WriteLine("Please enter a color you would like to see:");
                input = Console.ReadLine();
                formattedInput = char.ToUpper(input[0]) + input.Substring(1);

                if (formattedInput.Length >= 3)
                {
                    foreach (ConsoleColor color in Enum.GetValues(typeof(ConsoleColor)))
                    {
                        if (color.ToString() == formattedInput)
                        {
                            Console.ForegroundColor = color;
                            Console.WriteLine($"You picked {input} color and here it is");
                            Console.ForegroundColor = ConsoleColor.Gray;
                            notFound = false;
                            break;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Too short");
                }
            }
        } 
    }
}
