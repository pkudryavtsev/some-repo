﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorts
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] myArr = { 3, 5, 1, 7, 8, 10, 4 };

            QuickSort(myArr, 0, myArr.Length-1);

            foreach (var item in myArr)
            {
                Console.Write(item + " ");
            }

            Console.ReadKey();
        }

        static void QuickSort(int[] array, int low, int high)
        {
            if (low < high)
            {
                int mid = Partition(array, low, high);
                QuickSort(array, low, mid-1);
                QuickSort(array, mid + 1, high);
            }
        }

        static int Partition(int[] array, int low, int high)
        {
            int pivot = array[low];

            int i = low;
            int j = high;

            while (i < j)
            {
                while (array[i] <= pivot)
                {
                    i++;
                }
                

                do
                {
                    j--;
                }
                while (array[j] > pivot);

                if (i < j)
                {
                    var temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
            var tmp = array[low];
            array[low] = array[j];
            array[j] = tmp;

            return j;
        }
    }
}
