﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structures
{
    struct Train
    {
        string destination;
        int trainNumber;
        DateTime departureTime;

        public string Destination { get { return destination; } }
        public int TrainNumber { get { return trainNumber; } }
        public DateTime DepartureTime { get { return departureTime; } }


        public Train(string destination, int trainNumber, DateTime departureTime)
        {
            this.destination = destination;
            this.trainNumber = trainNumber;
            this.departureTime = departureTime;
        }
    }

    class MyClass
    {
        public string change;
    }

    struct MyStruct
    {
        public string change;
    }

    class Program
    {
        static void Main(string[] args)
        {
            CreateTrainSchedule();
            MyClass myClass = new MyClass();
            myClass.change = "not changed";

            MyStruct myStruct = new MyStruct();
            myStruct.change = "not changed";

            ClassTaker(myClass);
            StructTaker(ref myStruct);

            Console.WriteLine(myClass.change);
            Console.WriteLine(myStruct.change);
           

            Console.ReadKey();
        }

        static void CreateTrainSchedule()
        {
            CultureInfo provider = CultureInfo.InvariantCulture;

            Console.Write("Enter the number of trains:");

            int numOfTrains = int.Parse(Console.ReadLine());

            List<Train> trains = new List<Train>(numOfTrains);

            for (int i = 0; i < numOfTrains; i++)
            {
                Console.WriteLine("Please enter train details:");
                Console.Write($"\tTrain {i + 1}, Destination: ");
                string destination = Console.ReadLine();
                Console.Write($"\tTrain {i + 1}, Train Number: ");
                int trainNumber = int.Parse(Console.ReadLine());
                string format = "yyyy/MM/dd HH:mm";
                Console.Write($"\tTrain {i + 1}, Departure Time (Format: {format}): ");
                string departureTimeInput = Console.ReadLine();
                DateTime depatureTime = DateTime.ParseExact(departureTimeInput, format, provider);
                trains.Add(new Train(destination, trainNumber, depatureTime));
                Console.WriteLine("Succesfully added to schedule. Press key to continue...");
                Console.ReadKey();
                Console.Clear();
                trains.OrderBy(x => x.TrainNumber);
            }

            Console.Clear();
            Console.WriteLine("Please enter a train number to see info: ");
            int queryTrainNumber = int.Parse(Console.ReadLine());
            var foundTrain = trains.Where(x => x.TrainNumber == queryTrainNumber).ToList();

            if (foundTrain.Count != 0)
            {
                Console.WriteLine($"Train {foundTrain[0].TrainNumber} to {foundTrain[0].Destination} departs at {foundTrain[0].DepartureTime.ToString()}");
            }
            else
            {
                Console.WriteLine("Sorry, requested train was not found.");
            }
        }

        static void ClassTaker(MyClass myClass)
        {
            myClass.change = "changed";
        }

        static void StructTaker(ref MyStruct myStruct)
        {
            myStruct.change = "changed";
        }
    }
}
