﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

// Make peshku

namespace HWConsoleApp7_Methods
{
    class DecimalCounter
    {
        public int Counter { get; set; }

        public DecimalCounter()
        {
            Counter = 0;
        }

        public DecimalCounter(int counter)
        {
            Counter = counter;
        }

        public void IncrementCounter()
        {
            Counter++;
        }

        public void DecrementCounter()
        {
            Counter--;
        }
    }

    class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point()
        {
            X = 0;
            Y = 0;
        }

        public Point(int x, int y) 
        {
            X = x;
            Y = y;
        }

        public void ToDefault()
        {
            X = 0;
            Y = 0;
        }

        ~Point()
        {
            Console.WriteLine("Point object destroyed");
        }
    }

    abstract class BaseFigure
    {
        public Point Point { get; set; }

        public BaseFigure(int x, int y)
        {
            Point = new Point(x, y);
        }

        public virtual bool CanBeat(Point point)
        {
            throw new NotImplementedException();
        }

        public virtual bool CanBeat(int x, int y)
        {
            throw new NotImplementedException();
        }
    }

    class Rook : BaseFigure
    {
        public Rook(int x, int y) : base(x, y)
        {

        }

        public override bool CanBeat(Point point)
        {
            return CanBeat(point.X, point.Y);
        }

        public override bool CanBeat(int x, int y)
        {
            return (!(Point.X == x && Point.Y == y)) && ((Point.X == x) || (Point.Y == y));
        }
    }

    class Queen : BaseFigure
    {
        public Queen(int x, int y) : base(x, y)
        {

        }

        public override bool CanBeat(Point point)
        {
            return CanBeat(point.X, point.Y);
        }

        public override bool CanBeat(int x, int y)
        {
            return (!(Point.X == x && Point.Y == y)) && ((Point.X == x) || (Point.Y == y)) || (Math.Abs(Point.X - x) == Math.Abs(Point.Y - y));
        }
    }

    class Knight : BaseFigure
    {
        public Knight(int x, int y) : base(x, y)
        {

        }

        public override bool CanBeat(Point point)
        {
            return CanBeat(point.X, point.Y);
        }

        public override bool CanBeat(int x, int y)
        { 
            return Math.Abs((Point.X - x) * (Point.Y - y)) == 2;
        }
    }

    class Pawn : BaseFigure
    {
        public Pawn(int x, int y) : base(x, y)
        {

        }

        public override bool CanBeat(Point point)
        {
            return base.CanBeat(point);
        }

        public override bool CanBeat(int x, int y)
        {
            return (Math.Abs(Point.X - x) == 1) && ((y - Point.Y) == 1);
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            DecimalCounter dc = new DecimalCounter(3);

            CalcPodBoem();

            int[] A = { 2, 3, 7, 6, 5, 1, 0, 8 };

            MergSort(A, 0, A.Length - 1);

            for (int i = 0; i < A.Length; i++)
            {
                Console.Write(A[i].ToString() + " ");
            }

            Console.ReadKey();
        }

        static int CalcFactorial(int a)
        {
            return a != 1 ? a * CalcFactorial(a - 1) : 1;
        }

        static int CalcGCD(int a, int b)
        {
            return b != 0 ? CalcGCD(b, a % b) : a;
        }

        static string WriteBinaryRep(int a)
        {
            return a != 0 ? WriteBinaryRep(a >> 1) + (a % 2).ToString() : "";
        }

        static void CalcPodBoem()
        {
            string readPath = @"C:\Users\pc\source\repos\HWConsoleApp1\CalcPodBoemINPUT.txt";
            string writePath = @"C:\Users\pc\source\repos\HWConsoleApp1\CalcPodBoemOUTPUT.txt";
            int numOfPodBoem = 0;

            try
            {
                using (StreamReader sr = new StreamReader(readPath))
                {
                    string data = sr.ReadLine();

                    string newData = data.Replace('A', '1')
                                         .Replace('B', '2')
                                         .Replace('C', '3')
                                         .Replace('D', '4')
                                         .Replace('E', '5')
                                         .Replace('F', '6')
                                         .Replace('G', '7')
                                         .Replace('H', '8');

                    string[] newDataArr = newData.Split(' ');

                    string strRook = newDataArr[0];
                    string strQueen = newDataArr[1];
                    string strKnight = newDataArr[2];
                    string strPawn = "45";

                    Rook rook = new Rook(int.Parse(strRook[0].ToString()), int.Parse(strRook[1].ToString()));
                    Queen queen = new Queen(int.Parse(strQueen[0].ToString()), int.Parse(strQueen[1].ToString()));
                    Knight knight = new Knight(int.Parse(strKnight[0].ToString()), int.Parse(strKnight[1].ToString()));
                    Pawn pawn = new Pawn(int.Parse(strPawn[0].ToString()), int.Parse(strPawn[1].ToString()));

                    BaseFigure[] figures = { rook, queen, knight, pawn };

                    for(int x = 1; x <= 8; x++)
                    {
                        for (int y = 1; y <= 8; y++)
                        {
                            foreach (var figure in figures)
                            {
                                if (figure.CanBeat(x, y)) numOfPodBoem++;
                            }
                        }
                    }

                    Console.WriteLine(numOfPodBoem);
                }

                using (StreamWriter sw = new StreamWriter(writePath))
                {
                    sw.WriteLine(numOfPodBoem.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        static void CalcSalaryDifference()
        {
            string readPath = @"C:\Users\pc\source\repos\HWConsoleApp1\CalcSalaryDifferenceINPUT.txt";
            string writePath = @"C:\Users\pc\source\repos\HWConsoleApp1\CalcSalaryDifferenceOUTPUT.txt";
            int salaryDifference = 0;
            try
            {
                using (StreamReader sr = new StreamReader(readPath))
                {
                    string data = sr.ReadLine();

                    int[] typedInputArray = data.Split(' ').Select(x => Convert.ToInt32(x)).ToArray();

                    int maxSalary = typedInputArray.Max();
                    int minSalary = typedInputArray.Min();
                    salaryDifference = maxSalary - minSalary;
                }

                using (StreamWriter sw = new StreamWriter(writePath))
                {
                    sw.WriteLine(salaryDifference);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine(salaryDifference);
        }

        static void CalcSumOfDivisors()
        {
            string readPath = @"C:\Users\pc\source\repos\HWConsoleApp1\CalcSumOfDivisorsINPUT.txt";
            string writePath = @"C:\Users\pc\source\repos\HWConsoleApp1\CalcSumOfDivisorsOUTPUT.txt";
            int sumOfDivisors = 0;
            try
            {
                using (StreamReader sr = new StreamReader(readPath))
                {
                    int n = int.Parse(sr.ReadLine());
                    int divisor = n;
                    while (divisor != 0)
                    {
                        sumOfDivisors += ((n % divisor) == 0) ? divisor : 0;
                        divisor--;
                    }
                    sr.Close();
                }

                using (StreamWriter sw = new StreamWriter(writePath))
                {
                    sw.WriteLine(sumOfDivisors);
                    sw.Close();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine(sumOfDivisors);
        }

        static void QuickSort(int[] array, int low, int high)
        {
            if (low < high)
            {
                int partitionIndex = QuickSortPartition(array, low, high);

                QuickSort(array, low, partitionIndex - 1);
                QuickSort(array, partitionIndex + 1, high);
            }
        }

        static int QuickSortPartition(int[] array, int low, int high)
        {
            int pivot = array[high];

            int lowIndex = (low - 1);

            for (int j = low; j < high; j++)
            {
                if (array[j] <= pivot)
                {
                    lowIndex++;

                    int temp = array[lowIndex];
                    array[lowIndex] = array[j];
                    array[j] = temp;
                }
            }

            int temp1 = array[lowIndex + 1];
            array[lowIndex + 1] = array[high];
            array[high] = temp1;

            return lowIndex + 1;
        }

        static void Merg(int[] numbers, int left, int mid, int right)
        {
            int[] temp = new int[numbers.Length];

            int leftEnd = mid - 1;
            int tempCounter = left;
            int numOfValues = right - left + 1;

            while ((left <= leftEnd) && (mid <= right))
            {
                if (numbers[left] <= numbers[mid])
                {
                    temp[tempCounter++] = numbers[left++];
                }
                else
                {
                    temp[tempCounter++] = numbers[mid++];
                }
            }

            while (left <= leftEnd)
            {
                temp[tempCounter++] = numbers[left++];
            }

            while (mid <= right)
            {
                temp[tempCounter++] = numbers[mid++];
            }

            for (int i = 0; i < numOfValues; i++)
            {
                numbers[right] = temp[right];
                right--;
            }
        }

        static void MergSort(int[] numbers, int left, int right)
        {
            if (right > left)
            {
                int mid = (right + left) / 2;
                MergSort(numbers, left, mid);
                MergSort(numbers, mid + 1, right);

                Merg(numbers, left, mid + 1, right);
            }
        }


        static public void Merge(int[] numbers, int left, int mid, int right)
        {
            int[] temp = new int[numbers.Length];
            int i, left_end, num_elements, tmp_pos;

            left_end = (mid - 1);
            tmp_pos = left;
            num_elements = (right - left + 1);

            while ((left <= left_end) && (mid <= right))
            {
                if (numbers[left] <= numbers[mid])
                    temp[tmp_pos++] = numbers[left++];
                else
                    temp[tmp_pos++] = numbers[mid++];
            }

            while (left <= left_end)
                temp[tmp_pos++] = numbers[left++];

            while (mid <= right)
                temp[tmp_pos++] = numbers[mid++];

            //for (i = 0; i < num_elements; i++)
            //{
            //    numbers[right] = temp[right];
            //    right--;
            //}
        }

        static public void MergeSort(int[] numbers, int left, int right)
        {
            int mid;

            if (right > left)
            {
                mid = (right + left) / 2;
                MergeSort(numbers, left, mid);
                MergeSort(numbers, (mid + 1), right);

                Merge(numbers, left, (mid + 1), right);
            }
        }

        static void SelectionSort(int[] array)
        {
            for (int i = 0; i < array.Length - 1; i++)
            {
                int currentMin = i;
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[j] < array[currentMin])
                    {
                        currentMin = j;
                    }
                }

                if (currentMin != i)
                {
                    var temp = array[i];
                    array[i] = array[currentMin];
                    array[currentMin] = temp;
                }
            }
        }

        static void BubbleSort(int[] array)
        {
            for (int i = 0; i < array.Length - 1; i++)
            {
                for (int j = 0; j < array.Length - 1 - i; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        var temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
            }
        } 
    }
}
