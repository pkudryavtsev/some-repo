﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            SetBits();
            Task1();
            Task2();
            Task3();
            Task4();
            Task5();
            Task6();
            Task7();
            Task8();
            Task9();
            Task10();
            Task11();
            CalcSquare();
            CalcDivision();
            ConvertTo();

            Console.ReadKey();
        }

        static int Task1()
        {
            int a = 6;
            int b = (1 << a);
            Console.WriteLine(b);
            return b; 
        }

        static void Task2()
        {
            int a = 12;
            string result = string.Empty;
            
            while (a != 0)
            {
                result = result.Insert(0, (a % 2).ToString());
                a = a >> 1;
            }

            Console.WriteLine(result);
        }

        static void Task3()
        {
            int a = 112;
            int counter = 0;
            while (a != 0)
            {
                counter += a % 2;
                a = a >> 1;
            }

            Console.WriteLine(counter);
        }

        static void Task4()
        {
            ulong a = 123;

            int counter = 0;

            int co = 0;
            while(a != 0)
            {
                co++;
                a = a >> 1;
            }
            Console.WriteLine(counter-1);
        }

        static void Task5()
        {
            int n = 9;
            int i = 1;
            var res = n | (1 << i);
            Console.WriteLine(res);
        }

        static void Task6()
        {
            int n = 11;
            int i = 1;
            var res = n & (~(1 << i));
            Console.WriteLine(res);
        }

        static void Task7()
        {
            int n = 5;
            int i = 1;
            int res = (((n >> i) % 2) == 0) ? (n | (1 << i)) : (n & (~(1 << i)));
            Console.WriteLine(res);

        }

        static void Task8()
        {
            byte n = 65;
            byte b = (byte) ((byte)(n << 1) >> 1);
            n = (byte) ((b << 1) + ((b != n) ? 1 : 0));
            Console.WriteLine(n);
        }

        static void Task9()
        {
            //TODO Сделать через побитовые операции
            string str = "11110111";
            var arr = str.Split('0');

            int res = 0;
            foreach(var el in arr)
            {
                res += (el.Length - 1);
            }
            Console.WriteLine(res);

            int input = 243;

            int counter = 0;
            int prevValue = 0;
            while (input != 0)
            {
                if ((input % 2) == 1)
                {
                    int currentValue = 1;
                    if (currentValue == prevValue)
                    {
                        counter++;
                        
                    }
                    prevValue = currentValue;
                    input = input >> 1;
                }
                else 
                {
                    prevValue = 0;
                    input = input >> 1; 
                }

            }

            Console.WriteLine(counter);


        }

        static void Task10()
        {
            //TODO Попробовать сделать через перемену мест битов а не через условия
            int a = 5;  //1010
            int n = 0;   //0000
            int i = 1;   //0011
            int res = 0; //0000

            int x = (a >> n) % 2;
            int y = (a >> i) % 2;

            if (x == y) res = a;

            else if (x == 1 && y == 0)
            {
                res = ((a & ~(1 << n)) | (1 << i));
            }
            else if (x == 0 && y == 1)
            {
                res = ((a | (1 << n)) & (~(1 << i)));
            }

            Console.WriteLine(res);
        }

        static void Task11()
        {
            int a = 55;
            int n = 3;
            int result;
            int leftPart = ((a >> (n+1)) << n);
            int rightPart;
            int rightPartMask = 0;

            for (int i = 0; i < n; i++)
            {
                rightPartMask = rightPartMask | (1 << i);
            }

            Console.WriteLine(rightPartMask);

            rightPart = (a & rightPartMask);

            result = leftPart | rightPart;

            Console.WriteLine(result); 
        }

        static void SetBits()
        {
            int x = 27;
            int p = 2;
            int y = 55;
            int n = 5;

            int yMaskRight = 0;
            for (int i = 0; i < n; i++)
            {
                yMaskRight = yMaskRight | (1 << i);
            }
            int yRightPart = (y & yMaskRight) << p;

            int xMaskRight = 0;
            for (int i = 0; i < p; i++)
            {
                xMaskRight = xMaskRight | (1 << i);
            }
            int xRightPart = (x & xMaskRight);
            int xLeftPart = (x >> (p + n)) << (p + n);

            int res = xRightPart | yRightPart | xLeftPart;

            Console.WriteLine(res);
        }

        static void Task12()
        {
            // x = 0000 0111
            // x - 1 = 0000 0110
            // 0000 0111 &
            // 0000 0110
            // 0000 0110

            // x = 0000 0110
            // x - 1 = 0000 0101
            // 0000 0110 &
            // 0000 0101
            // 0000 0100
        }

        static void Task13()
        {
            // x = 0000 1010 &
            // -x = 1111 0110
            // 0000 0010

            // 0001 1100 &
            // 1110 0001
            // 0000 0000
        }

        static void Task14()
        {
            // 0011 0110 |
            // 0000 0001
        }

        static void Task15()
        {
            // 0011 0101 |  
            //~1100 1001
            // 1111 1111 &
            // 1111 1110
            // 1111 1110

        }

        static void CalcSquare()
        {
            Console.WriteLine("Please enter your number ending with 5:");
            string input = Console.ReadLine();
            
            if (input.EndsWith("5"))
            {
                int typedInput = int.Parse(new string(input.Take(input.Length - 1).ToArray()));
                int tempNum = typedInput * (typedInput + 1);
                string tempStr = tempNum.ToString();
                string result = tempStr + "25";
                Console.WriteLine(result);
            }
            else Console.WriteLine("Please enter a number ending with 5");
        }

        static void CalcDivision()
        {
            int dividend, divisor, quotient, remainder;

            Console.WriteLine("Please enter dividend:");
            string dividendInput = Console.ReadLine();
            dividend = int.Parse(dividendInput);

            Console.WriteLine("Please enter a divisor:");
            string divisorInput = Console.ReadLine();
            divisor = int.Parse(divisorInput);

            if (divisor != 0)
            {
                remainder = dividend % divisor;
                quotient = dividend / divisor;
                if (remainder == 0)
                {
                    Console.WriteLine("Successfuly divided without a remainder!");
                }
                else
                {
                    Console.WriteLine("Divided with remainder.");
                    Console.WriteLine($"Your remainder is: {remainder}");
                }
                Console.WriteLine($"Your quotient is: {quotient}");
            }
            else
            {
                Console.WriteLine("You cannot divide by zero!");
            }
        }

        static void ConvertTo()
        {
            double conversionNum, result;
            string conversionNumInput, conversionType;
            
            Console.WriteLine("Please a number for conversion:");
            conversionNumInput = Console.ReadLine();
            conversionNum = double.Parse(conversionNumInput);

            Console.WriteLine($"Pick conversion type: {Environment.NewLine}" +
                $"Enter 'k' to convert to kilobytes;{Environment.NewLine}" +
                $"Enter 'b' to convert to bytes.");
            conversionType = Console.ReadLine().ToLower();

            switch (conversionType)
            {
                case "k":
                    result = (double) conversionNum / 1024;
                    Console.WriteLine($"Converted amount is: {result:F2} kilobytes");
                    break;
                case "b":
                    result = (double) conversionNum * 1024;
                    Console.WriteLine($"Converted amount is: {result:F2} bytes");
                    break;
                default:
                    Console.WriteLine("You haven't selected the right option. Try again.");
                    break;
            }
        }
    }
}
