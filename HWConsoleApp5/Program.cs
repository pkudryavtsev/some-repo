﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            StringFormatting();
            CalcThousands();
            EquationSolver(3, -9);
            AvgCalc(5, 45);
            DistanceCalc(0, 0, 5, 4);
            Cycle();

            Console.ReadKey();
        }

        static void BinaryCalc()
        {
            //1) 0110 1101 << 1
            //0110 1101 |
            //0001 0010
            //0111 1111 &
            //1011 1111
            
            //2) 0011 0100 << 2
        }

        static void StringFormatting()
        {
            int a = 1, b = 13, c = 49;

            string s = string.Format("{0} {1} {2}", a, b, c);

            Console.WriteLine($"{a} {b} {c}");
            Console.WriteLine("{0} {1} {2}", a, b, c);
            Console.WriteLine(s);
        }

        static void CalcThousands()
        {
            double a = 1234.48928593;
            Console.WriteLine($"{a:F3}");
        }

        static void EquationSolver(double a, double b)
        {
            double x = (-b) / a;
            Console.WriteLine(x);
        }

        static void AvgCalc(double a, double b)
        {
            a = Math.Abs(a);
            b = Math.Abs(b);
            var avg = (a + b) / 2;
            var geomAvg = Math.Sqrt(a * b);
            Console.WriteLine(avg);
            Console.WriteLine(geomAvg);
        }

        static void DistanceCalc(int x1, int y1, int x2, int y2)
        {
            var distance = Math.Sqrt(Math.Pow((x2 - x1), 2) + Math.Pow((y2 - y1), 2));

            Console.WriteLine(distance);
        }

        static void Cycle()
        {
            int[] a = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
            int s = 0;
            Console.WriteLine("Enter K");
            string userInput = Console.ReadLine();
            int k = int.Parse(userInput);
            for (int i = 0; i < a.Length; i++)
            {
                if (s < k)
                {
                    s += a[i];
                }
                else
                {
                    break;
                }
                Console.WriteLine(s);
            }
            Console.WriteLine("Final");
            Console.WriteLine(s);
        }
    }
}
