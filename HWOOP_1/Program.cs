﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWOOP_1
{
    class DynamicArray
    {
        public int[] Array;

        public int this[int index]
        {
            get
            {
                return Array[index];
            }
            set
            {
                Array[index] = value;
            }
        }

        public DynamicArray(int length = 0)
        {
            Array = new int[length];
        }

        public void Fill()
        {
            Random rand = new Random();
            for (int i = 0; i < Array.Length; i++)
            {
                Array[i] = rand.Next(10);
            }
        }

        public int FirstIndexOf(int x, int startIndex = 0)
        {
            for (int i = startIndex; i < Array.Length; i++)
            {
                if (Array[i] == x) return startIndex;
            }
            return -1;
        }

        public int LastIndexOf(int x, int startIndex = 0)
        {
            for (int i = Array.Length-1; i >= startIndex ; i--)
            {
                if (Array[i] == x) return startIndex;
            }
            return -1;
        }

        public void AddFirst(int x)
        {
            int[] temp = Array;
            Array = new int[Array.Length + 1];
            for (int i = 1; i < Array.Length; i++)
            {
                Array[i] = temp[i - 1];
            }
            Array[0] = x;
        }

        public void AddLast(int x)
        {
            int[] temp = Array;
            Array = new int[Array.Length + 1];
            for (int i = 0; i < Array.Length - 1; i++)
            {
                Array[i] = temp[i];
            }
            Array[Array.Length - 1] = x;

        }

        public void RemoveByIndex(int index)
        {
            int[] temp = new int[Array.Length - 1];

            for (int i = 0; i < index; i++)
            {
                temp[i] = Array[i];
            }

            for (int j = index + 1; j < Array.Length; j++)
            {
                temp[j - 1] = Array[j];
            }

            Array = temp;
        }

        public void ReversePartial(int startIndex, int length)
        {
            int[] temp = new int[length];

            for (int i = 0; i < temp.Length; i++)
            {
                temp[i] = Array[startIndex + i];
            }

            int j = length - 1;
            for (int i = 0; i < length / 2; i++)
            {
                var tempValue = temp[i];
                temp[i] = temp[j];
                temp[j] = tempValue;
                j--;
            }

            for (int i = 0; i < length; i++)
            {
                Array[startIndex + i] = temp[i];
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        ~DynamicArray() {
            Console.WriteLine("Object was destroyed");
        }
    }

    public static class AnimalExtension
    {
        public static void SortByAmountOfFoodAndName(this List<Animal> animals)
        {
            animals.Sort((a, b) =>
            {
                var firstCompare = a.AmountOfFood.CompareTo(b.AmountOfFood);
                return firstCompare != 0 ? firstCompare : a.Name.CompareTo(b.Name);
            });
        }

        public static List<Animal> SortByAmountOfFoodAndNameLINQ(this List<Animal> animals)
        {
            return animals.OrderBy(obj => obj.AmountOfFood).ThenBy(obj => obj.Name).ToList();
        }

        public static void DisplayFirstFiveNames(this List<Animal> animals)
        {
            for (int i = 0; i < 5; i++)
            {
                Console.Write(animals[i].Name + " ");
            }
            Console.WriteLine();
        }

        public static void DisplayLastThreeIDs(this List<Animal> animals)
        {
            for (int i = animals.Count - 3; i < animals.Count; i++)
            {
                Console.Write(animals[i].Id + " ");
            }
            Console.WriteLine();
        }
    }

    public abstract class Animal
    {
        public int Id { get; }
        public string Name { get; }
        public double Weight { get; }
        public double AmountOfFood { get; }

        public Animal(int id, string name, double weight)
        {
            Id = id;
            Name = name;
            Weight = weight;
            AmountOfFood = CalculateFood(weight);
        }

        public virtual double CalculateFood(double weight)
        {
            throw new NotImplementedException();
        }
    }

    class Predaceous : Animal
    {
        public string Type { get; } = "Predaceous";

        public Predaceous(int id, string name, double weight) : base(id, name, weight)
        {

        }

        public override double CalculateFood(double weight)
        {
            return weight * 0.75;
        }
    }

    class Omnivorous : Animal
    {
        public string Type { get; } = "Omnivorous";

        public Omnivorous(int id, string name, double weight) : base(id, name, weight)
        {
        }

        public override double CalculateFood(double weight)
        {
            return weight * 1;
        }
    }

    class Herbivorous : Animal
    {
        public string Type { get; } = "Herbivorous";

        public Herbivorous(int id, string name, double weight) : base(id, name, weight)
        {
        }

        public override double CalculateFood(double weight)
        {
            return weight * 1.25;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Predaceous predator = new Predaceous(1, "Tom", 10);
            Omnivorous omnivorous = new Omnivorous(2, "Igor", 6);
            Herbivorous herbivorous = new Herbivorous(3, "Max", 15);
            Omnivorous omnivorous1 = new Omnivorous(4, "Ian", 6);
            Predaceous predator1 = new Predaceous(5, "Anton", 7);
            Herbivorous herbivorous1 = new Herbivorous(6, "Masha", 10);

            List<Animal> animals = new List<Animal>();
            animals.Add(predator);
            animals.Add(omnivorous);
            animals.Add(herbivorous);
            animals.Add(omnivorous1);
            animals.Add(predator1);
            animals.Add(herbivorous1);

            animals.SortByAmountOfFoodAndName();

            animals.DisplayFirstFiveNames();

            animals.DisplayLastThreeIDs();

            Console.ReadKey();
        }
    }
}
