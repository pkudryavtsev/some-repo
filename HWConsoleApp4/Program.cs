﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculatePremium(10503, 17);
            Console.ReadKey();
        }

        static void CalculatePremium(double salary, int yearsOfExperience)
        {
            if (yearsOfExperience < 5)
            {
                Console.WriteLine(salary * 0.1);
            }
            else if (yearsOfExperience < 10) 
            {
                Console.WriteLine(salary * 0.15);
            }
            else if (yearsOfExperience < 15)
            {
                Console.WriteLine(salary * 0.25);
            }
            else if (yearsOfExperience < 20)
            {
                Console.WriteLine(salary * 0.35);
            }
            else if (yearsOfExperience < 25)
            {
                Console.WriteLine(salary * 0.45);
            }
            else
            {
                Console.WriteLine(salary * 0.5);
            }


        }
    }
}
