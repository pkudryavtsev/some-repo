﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    class Program
    {
        public delegate double Delegate1(double a, double b);
        public delegate double Delegate2(Delegate[] delegates);
        public delegate int RandDelegate();

        static void Main(string[] args)
        {
            AverageOfDelegateArray();

            Console.Read();            
        }

        static void PerformArithmetic()
        {
            Delegate1 Add = (a, b) => { return a + b; };
            Delegate1 Sub = (a, b) => { return a - b; };
            Delegate1 Mul = (a, b) => { return a * b; };
            Delegate1 Div = (a, b) => { return a / b; };

            while (true)
            {
                double operand1, operand2;
                Console.Write("Please enter first number: ");
                operand1 = double.Parse(Console.ReadLine());

                Console.Write("Please enter second number: ");
                operand2 = double.Parse(Console.ReadLine());

                Console.WriteLine("Please choose an operation: addition (add), subtraction (sub), division (div) or multiplication (mul)");
                string input = Console.ReadLine();

                switch (input)
                {
                    case "add":
                        Console.WriteLine($"Result is {Add.Invoke(operand1, operand2)}");
                        break;
                    case "sub":
                        Console.WriteLine($"Result is {Sub.Invoke(operand1, operand2)}");
                        break;
                    case "mul":
                        Console.WriteLine($"Result is {Mul.Invoke(operand1, operand2)}");
                        break;
                    case "div":
                        if (operand2 == 0)
                        {
                            Console.WriteLine("Cannot divide by zero!");
                            Console.ReadKey();
                        }
                        else
                        {
                            Console.WriteLine($"Result is {Div.Invoke(operand1, operand2)}");
                        };
                        break;
                    default:
                        break;
                }

                Console.Clear();
            }
        }

        static void AverageOfDelegateArray()
        {
            Random random = new Random();

            RandDelegate randDelegate1 = () => random.Next(1, 20);
            RandDelegate randDelegate2 = () => random.Next(1, 20);
            RandDelegate randDelegate3 = () => random.Next(1, 20);
            RandDelegate randDelegate4 = () => random.Next(1, 20);

            RandDelegate[] delegates = new RandDelegate[] { randDelegate1, randDelegate2, randDelegate3, randDelegate4 };

            Delegate2 averageDelegate = (dlgs) =>
            {
                int sum = 0;
                for (int i = 0; i < delegates.Length; i++)
                {
                    sum += delegates[i].Invoke();
                }
                return sum / delegates.Length;
            };

            Console.WriteLine(averageDelegate.Invoke(delegates));
        }
    }
}
