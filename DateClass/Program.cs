﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DateClass
{
    enum DayOfWeekEnum
    {
        Sunday = 0,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday
    }
   
    class DateClass
    {
        static int[] daysInMonth = new int[] {31, 28, 31, 30, 31, 30,
                                              31, 31, 30, 31, 30, 31};

        long seconds;

        public long Seconds
        {
            get { return seconds; }
        }

        public int Second
        {
            get { return CalculateNumOfSeconds(seconds); }
        }

        public int Minute
        {
            get { return CalculateNumOfMinutes(seconds); }
        }

        public int Hour
        {
            get { return CalculateNumOfHours(seconds); }
        }

        public int Day
        {
            get { return CalculateNumOfDays(seconds); }
        }

        public int Month 
        {
            get { return CalculateNumOfMonths(seconds); } 
        }

        public int Year 
        {
            get { return CalculateNumOfYears(seconds); }
        }

        public string DayOfWeek
        {
            get { return CalculateDayOfWeek(seconds); }
        }

        private DateClass(long _seconds)
        {
            seconds = _seconds;
        }

        public DateClass(int day, int month, int year)
        {
            seconds = CalculateSeconds(0, 0, 0, day, month, year);
        }

        public DateClass(int second, int minute, int hour, int day, int month, int year)
        {
            seconds = CalculateSeconds(second, minute, hour, day, month, year);
        }

        private long CalculateSeconds(int second, int minute, int hour, int day, int month, int year)
        {
            return second + minute * 60 + hour * 60 * 60 + CalculateDays(day, month, year) * 24 * 60 * 60;
        }

        private int CalculateNumOfSeconds(long seconds)
        {
            int years;
            for (years = 0; seconds - CalculateYearLength(years) > 0; years++)
            {
                seconds -= CalculateYearLength(years);
            }

            int months;
            for (months = 0; seconds > 0; months++)
            {
                seconds -= (months == 1 && isLeapYear(years) ? 29 : daysInMonth[months]) * 24 * 60 * 60;
            }
            seconds += daysInMonth[months] * 24 * 60 * 60;

            long days = seconds / 24 / 60 / 60;

            seconds -= days * 24 * 60 * 60;

            long hours = seconds / 60 / 60;

            seconds -= hours * 60 * 60;

            long minutes = seconds / 60;

            seconds -= minutes * 60;

            long retSeconds = seconds;
            return (int)retSeconds;
        }

        private int CalculateNumOfMinutes(long seconds)
        {
            int years;
            for (years = 0; seconds - CalculateYearLength(years) > 0; years++)
            {
                seconds -= CalculateYearLength(years);
            }

            int months;
            for (months = 0; seconds > 0; months++)
            {
                seconds -= (months == 1 && isLeapYear(years) ? 29 : daysInMonth[months]) * 24 * 60 * 60;
            }
            seconds += daysInMonth[months] * 24 * 60 * 60;

            long days = seconds / 24 / 60 / 60;

            seconds -= days * 24 * 60 * 60;

            long hours = seconds / 60 / 60;

            seconds -= hours * 60 * 60;

            long minutes = seconds / 60;
            return (int)minutes;
        }

        private int CalculateNumOfHours(long seconds)
        {
            int years;
            for (years = 0; seconds - CalculateYearLength(years) > 0; years++)
            {
                seconds -= CalculateYearLength(years);
            }

            int months;
            for (months = 0; seconds > 0; months++)
            {
                seconds -= (months == 1 && isLeapYear(years) ? 29 : daysInMonth[months]) * 24 * 60 * 60;
            }
            seconds += daysInMonth[months] * 24 * 60 * 60;

            long days = seconds / 24 / 60 / 60;

            seconds -= days * 24 * 60 * 60;

            long hours = seconds / 60 / 60;
            return (int)hours;
        }

        private int CalculateNumOfDays(long seconds)
        {
            int years;
            for (years = 0; seconds - CalculateYearLength(years) > 0; years++)
            {
                seconds -= CalculateYearLength(years);
            }

            int months;
            for (months = 0; seconds > 0; months++)
            {
                seconds -= (months == 1 && isLeapYear(years) ? 29 : daysInMonth[months]) * 24 * 60 * 60;
            }
            seconds += daysInMonth[months] * 24 * 60 * 60;

            long days = seconds / 24 / 60 / 60;
            return (int)days;
        }

        private int CalculateNumOfMonths(long seconds)
        {
            int years;
            for (years = 0; seconds - CalculateYearLength(years) > 0; years++)
            {
                seconds -= CalculateYearLength(years);
            }

            int months;
            for (months = 0; seconds > 0; months++)
            {
                seconds -= (months == 1 && isLeapYear(years) ? 29 : daysInMonth[months]) * 24 * 60 * 60;
            }
            return months;
        }

        private int CalculateNumOfYears(long seconds)
        {
            int years;
            for (years = 0; seconds - CalculateYearLength(years) > 0; years++)
            {
                seconds -= CalculateYearLength(years);
            }
            return years;
        }

        private string CalculateDayOfWeek(long seconds)
        {
            long days = seconds / 24 / 60 / 60;

            return Enum.GetName(typeof(DayOfWeekEnum), days % 7);
        }

        private int CalculateYearLength(int currentYear)
        {
            return isLeapYear(currentYear) ? 366 * 24 * 60 * 60 : 365 * 24 * 60 * 60;
        }
           
        private bool isLeapYear(int year)
        {
            return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
        }

        private long CalculateDays(int day, int month = 6, int year = 2000)
        {
            long daysCount = day;
            bool leapYear = isLeapYear(year);

            for (int i = 0; i < month - 1; i++)
            {
                daysCount += (leapYear && i == 1) ? 29 : daysInMonth[i];
            }

            for (int i = year - 1; i >= 0; i--)
            {
                daysCount += CalculateYearLength(i) / 24 / 60 / 60;
            }
            return daysCount;
        }

        public static DateClass operator +(DateClass date, int days)
        {
            long daysInSeconds = days * 24 * 60 * 60;
            long newSeconds = date.Seconds + daysInSeconds;
            return new DateClass(newSeconds);
        }

        public static DateClass operator -(DateClass date, int days)
        {
            long daysInSeconds = days * 24 * 60 * 60;
            long newSeconds = date.Seconds - daysInSeconds;
            return new DateClass(newSeconds);
        }

        public static bool operator ==(DateClass date1, DateClass date2)
        {
            return (date1.Day == date2.Day) && (date1.Month == date2.Month) && (date1.Year == date2.Year);
        }

        public static bool operator !=(DateClass date1, DateClass date2)
        {
            return (date1.Day != date2.Day) || (date1.Month != date2.Month) || (date1.Year != date2.Year);
        }

        public static bool operator >(DateClass date1, DateClass date2)
        {
            if (date1.Year > date2.Year) return true;
            else if (date1.Year == date2.Year)
            {
                if (date1.Month > date2.Month) return true;
                else if (date1.Month == date2.Month)
                {
                    return date1.Day > date2.Day;
                }
            }
            return false;
        }

        public static bool operator <(DateClass date1, DateClass date2)
        {
            if (date1.Year < date2.Year) return true;
            else if (date1.Year == date2.Year)
            {
                if (date1.Month < date2.Month) return true;
                else if (date1.Month == date2.Month)
                {
                    return date1.Day < date2.Day;
                }
            }
            return false;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            DateClass date = new DateClass(0, 4, 19, 28, 6, 2020);

            Console.WriteLine(date.Year);
            Console.WriteLine(date.Day);
            Console.WriteLine(date.Month);
            Console.WriteLine(date.Hour);
            Console.WriteLine(date.Minute);
            Console.WriteLine(date.Second);
            Console.WriteLine(date.DayOfWeek);

            DateClass newDate = date + 4;

            Console.WriteLine(newDate.Year);
            Console.WriteLine(newDate.Month);
            Console.WriteLine(newDate.Day);
            Console.WriteLine(newDate.Hour);
            Console.WriteLine(newDate.Minute);
            Console.WriteLine(newDate.Second);
            Console.WriteLine(newDate.DayOfWeek);

            Console.ReadKey();
        }
    }
}
