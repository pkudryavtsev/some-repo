﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            double x = 6;
            double y = 9;

            int a = 2;
            int b = 3;
            int c = 5;

            Console.ReadKey();
        }

        static bool CalculateIfOnCircle1(int x, int y)
        {
            int r = 2;

            return Math.Abs(x * x + y * y - r * r) <= Math.Pow(10, -3);
        }

        static bool CalculateIfInTriangle2(double x, double y)
        {
            return (y >= 0 && (y + Math.Abs(x)) <= 1);
        }

        static int GetMaxOfTwoNums3(int a, int b)
        {
            return a > 0? ((a > b) ? a : b) : ((a < b) ? a : b);
        }

        static int CalculateByCondition4(int a, int b, int c)
        {
            int minOfAB = (a > b) ? b : a;
            return minOfAB > c ? minOfAB : c;
        }

        static bool СalculateIfSquareInCircle5(double r, double s)
        {
            var radius = Math.Sqrt((2 * r) / Math.PI);
            var diagonal = Math.Sqrt(s) * Math.Sqrt(2);

            return radius >= diagonal / 2;
        }
        
        static bool CalcualteIfCircleInSquare6(double r, double s)
        {
            var radius = Math.Sqrt((2 * r) / Math.PI);
            var side = Math.Sqrt(s);

            return radius <= side / 2;
        }

        static double CalculateFunctionByCondition7(double x)
        {
            return Math.Abs(x) > 1 ? 1 : Math.Abs(x);
        }

        static double CalculateFunctionByCondition8(double x)
        {
            return Math.Abs(x) >= 1 ? 0 : x * x - 1;
        }

        static double CalculateFunctionByCondition9(double x)
        {
            return x <= -1 ? 0 : (x > 0 ? 1 : 1 + x);
        }

        static double CalculateFunctionByCondition10(double x)
        {
            return x <= -1 ? 1 : (x > 1 ? -1 : -x);
        }
    }

}
