﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Task2;
using Task3;
using Task4;

namespace Task2
{
    class ClassRoom
    {
        public List<Pupil> Pupils { get; set; } = new List<Pupil>();

        public ClassRoom(params Pupil[] pupils)
        {
            Pupils.AddRange(pupils.ToList());
        }
    }

    class Pupil
    {
        public Pupil() { }

        public virtual void Study()
        {
        }

        public virtual void Read()
        {
        }

        public virtual void Write()
        {
        }

        public virtual void Relax()
        {
        }
    }

    class ExcellentPupil : Pupil
    {
        public ExcellentPupil()
        {
        }

        public override void Study()
        {
            Console.WriteLine("I am studying very well"); ;
        }

        public override void Read()
        {
            Console.WriteLine("I am reading very well"); ;
        }

        public override void Write()
        {
            Console.WriteLine("I am writing very well");
        }

        public override void Relax()
        {
            Console.WriteLine("I am relaxing by playing chess");
        }
    }

    class GoodPupil : Pupil
    {
        public GoodPupil()
        {
        }

        public override void Study()
        {
            Console.WriteLine("I am studying OK");
        }

        public override void Read()
        {
            Console.WriteLine("I am reading OK");
        }

        public override void Write()
        {
            Console.WriteLine("I am writing OK");
        }

        public override void Relax()
        {
            Console.WriteLine("I am relaxing by doing sports");
        }
    }

    class BadPupil : Pupil
    {
        public BadPupil()
        {
        }

        public override void Study()
        {
            Console.WriteLine("I am bad at studying");
        }

        public override void Read()
        {
            Console.WriteLine("I am bad at reading");
        }

        public override void Write()
        {
            Console.WriteLine("I am bad at writing");
        }

        public override void Relax()
        {
            Console.WriteLine("I am relaxing when playing video games");
        }
    }
}

namespace Task3
{
    class Vehicle
    {
        public int X { get; }
        public int Y { get; }

        public int Price { get; }
        public int Speed { get; }
        public int YearOfProduction { get; }

        public Vehicle(int price, int speed, int yearOfProduction, int x, int y)
        {
            X = x;
            Y = y;
            Price = price;
            Speed = speed;
            YearOfProduction = yearOfProduction;
        }

        public virtual void ShowInfo()
        {
            Console.WriteLine($"The price of vehicle is {Price}, speed is {Speed} and produced in {YearOfProduction}");
        }
    }

    class Plane : Vehicle 
    {
        public int MaxHeight { get; }
        public int Capacity { get; }

        public Plane(int price, int speed, int yearOfProduction, int maxHeight, int capacity, int x = 0, int y = 0) 
            : base(price, speed, yearOfProduction, x, y)
        {
            MaxHeight = maxHeight;
            Capacity = capacity;
        }

        public override void ShowInfo()
        {
            Console.WriteLine($"The plane can reach {MaxHeight} meters and carry {Capacity}." +
                $" It's price is {Price}, speed is {Speed} and produced in {YearOfProduction}");
        }
    }

    class Car : Vehicle 
    {
        public Car(int price, int speed, int yearOfProduction, int x = 0, int y = 0) 
            : base(price, speed, yearOfProduction, x, y)
        {
        }

        public override void ShowInfo()
        {
            Console.WriteLine($"The price of a car is {Price}, speed is {Speed} and produced in {YearOfProduction}");
        }
    }

    class Ship : Vehicle 
    {
        public int Capacity { get; }
        public string PortOfOrigin { get; }

        public Ship(int price, int speed, int yearOfProduction, int capacity, string portOfOrigin, int x = 0, int y = 0) 
            : base(price, speed, yearOfProduction, x, y)
        {
            Capacity = capacity;
            PortOfOrigin = portOfOrigin;
        }

        public override void ShowInfo()
        {
            Console.WriteLine($"The ship is from {PortOfOrigin} and can carry {Capacity}." +
                $" It's price is {Price}, speed is {Speed} and produced in {YearOfProduction}");
        }
    } 
}

namespace Task4
{
    class DocumentWorker
    {
        public DocumentWorker()
        {
        }

        public virtual void OpenDocument()
        {
            Console.WriteLine("Document is open");
        }

        public virtual void EditDocument()
        {
            Console.WriteLine("Editing function is available in Pro version");
        }

        public virtual void SaveDocument()
        {
            Console.WriteLine("Saving functions is available in Pro version");
        }
    }

    class ProDocumentWorker : DocumentWorker
    {
        public ProDocumentWorker()
        {
        }

        public override void EditDocument()
        {
            Console.WriteLine("Document is edited");
        }

        public override void SaveDocument()
        {
            Console.WriteLine("Document is saved in old format, saving in other formats is available in Expert version");
        }
    }

    class ExpertDocumentWorker : ProDocumentWorker
    {
        public ExpertDocumentWorker()
        {
        }

        public override void SaveDocument()
        {
            Console.WriteLine("Document is saved in new format");
        }
    }
}

namespace OOPPractice
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.ReadKey();
        }

        static void ShowTask2()
        {
            ExcellentPupil ep = new ExcellentPupil();
            ExcellentPupil ep1 = new ExcellentPupil();
            GoodPupil gp = new GoodPupil();
            GoodPupil gp1 = new GoodPupil();
            BadPupil bp = new BadPupil();
            BadPupil bp1 = new BadPupil();

            Pupil[] pupils = new Pupil[] { ep, ep1, gp, gp1, bp, bp1 };

            ClassRoom classRoom = new ClassRoom(pupils);

            foreach (var pupil in classRoom.Pupils)
            {
                pupil.Study();
                pupil.Read();
                pupil.Write();
                pupil.Relax();
            }
        }

        static void ShowTask3()
        {
            List<Vehicle> vehicles = new List<Vehicle>();
            Plane plane = new Plane(1000000, 500, 1989, 3000, 60);
            Car car = new Car(30000, 220, 2016);
            Ship ship = new Ship(5000000, 20, 1970, 450, "Rotterdam");

            vehicles.Add(plane);
            vehicles.Add(car);
            vehicles.Add(ship);

            foreach(var vehicle in vehicles)
            {
                vehicle.ShowInfo();
            }
        }

        static void ShowTask4(string authLevel)
        {
            if (authLevel == "pro")
            {
                DocumentWorker pdw = new ProDocumentWorker();
            }
            else if (authLevel == "exp")
            {
                DocumentWorker edw = new ExpertDocumentWorker();
            }
            else
            {
                DocumentWorker sdw = new DocumentWorker();
            }
        }
    }
}
