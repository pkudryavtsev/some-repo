﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.XPath;
using System.Reflection;

namespace BinarySearchTree_Reworked
{
    class Program
    {
        public class Book
        {
            public int ID { get; }
            public string Author { get; }
            public string Title { get; }
            public int YearOfPublishing { get; }
            public int Quantity { get; set; }

            public Book(int id, string author, string title, int yearOfPublishing, int quantity)
            {
                ID = id;
                Author = author;
                Title = title;
                YearOfPublishing = yearOfPublishing;
                Quantity = quantity;
            }
        }

        public class Node
        {
            public Book Data { get; set; }
            public Node Right { get; set; }
            public Node Left { get; set; }
            public int Key => Data.ID;

            public Node(Book book)
            {
                Data = book;
            }
        }

        public class BinaryTree
        {
            public Node root;
            public BinaryTree() {}

            public void Insert(Book book)
            {
                Node node = new Node(book);
                if (root != null)
                {
                    InsertRecursive(root, node);
                }
                else
                {
                    root = node;
                }
            }

            public Book FindByKey(int key)
            {
                if (root.Key != key)
                {
                    return FindByKeyRecursive(root, key);
                }
                else
                {
                    return root.Data;
                }
            }

            public bool RemoveByKey(int key)
            {
                if (root != null)
                {
                    return RemoveByKeyRecursive(key, root);
                }
                else
                {
                    return false;
                }
            }

            public int ShowAvailability(int key)
            {
                Book requestedBook = FindByKey(key);

                return requestedBook != null ? requestedBook.Quantity : 0;
            }

            public void InOrderTraversal()
            {
                if (root != null)
                {
                    InOrderTraversalRecursive(root);
                }
            }

            private void InsertRecursive(Node currentNode, Node node)
            {
                if (node.Key < currentNode.Key)
                {
                    if (currentNode.Left == null)
                    {
                        currentNode.Left = node;
                    }
                    else
                    {
                        currentNode = currentNode.Left;
                        InsertRecursive(currentNode, node);
                    }
                }
                else if (node.Key >= currentNode.Key)
                {
                    if (currentNode.Right == null)
                    {
                        currentNode.Right = node;
                    }
                    else
                    {
                        currentNode = currentNode.Right;
                        InsertRecursive(currentNode, node);
                    }
                }
            }

            private Book FindByKeyRecursive(Node currentNode, int key)
            {
                if (currentNode.Key != key)
                {
                    if (key < currentNode.Key)
                    {
                        currentNode = currentNode.Left;
                        return FindByKeyRecursive(currentNode, key);
                    }
                    else if (key >= currentNode.Key)
                    {
                        currentNode = currentNode.Right;
                        return FindByKeyRecursive(currentNode, key);
                    }
                }
                else
                {
                    return currentNode.Data;
                }
                return null;
            }

            private bool RemoveByKeyRecursive(int key, Node startNode, Node parentNode = null)
            {

                if (startNode == null)
                {
                    return false;
                }
                else if (startNode == root && startNode.Key == key)
                {
                    root = GetSuccessor(root);
                    return true;
                }
                else if (startNode.Key == key)
                {
                    bool isLeftChild = parentNode.Left == startNode;
                    if (isLeftChild)
                    {
                        parentNode.Left = GetSuccessor(parentNode.Left);
                    }
                    else
                    {
                        parentNode.Right = GetSuccessor(parentNode.Right);
                    }
                    return true;
                }
                else if (startNode.Key > key)
                {
                    return RemoveByKeyRecursive(key, startNode.Left, startNode);
                }
                else
                {
                    return RemoveByKeyRecursive(key, startNode.Right, startNode);
                }
            }

            private Node GetSuccessor(Node nodeToDelete)
            {
                if (nodeToDelete.Left == null && nodeToDelete == null)
                {
                    return null;
                }
                else if (nodeToDelete.Left == null)
                {
                    return nodeToDelete.Right;
                }
                else if (nodeToDelete.Right == null)
                {
                    return nodeToDelete.Left;
                }
                else
                {
                    Node replacementNode = GetReplacementNode(nodeToDelete.Right);
                    replacementNode.Left = nodeToDelete.Left;
                    replacementNode.Right = nodeToDelete.Right;
                    return replacementNode;
                }
            }

            private Node GetReplacementNode(Node startNode, Node parentNode = null)
            {
                if (startNode.Left != null)
                {
                    return GetReplacementNode(startNode.Left, startNode);
                }
                else
                {
                    parentNode.Left = null;
                    return startNode;
                }
            }

            //private void RemoveByKeyRecursive(int key)
            //{
            //    Node currentNode = root;
            //    Node parentNode = root;
            //    bool isLeftChild = false;

            //    if (currentNode == null) 
            //    {
            //        Console.WriteLine("Nothing to remove in the tree. No elements are there.");
            //        return; 
            //    }

            //    while (currentNode.Key != key && currentNode != null)
            //    {
            //        parentNode = currentNode;
            //        if (key < currentNode.Key)
            //        {
            //            currentNode = currentNode.Left;
            //            isLeftChild = true;
            //        }
            //        else if (key >= currentNode.Key)
            //        {
            //            currentNode = currentNode.Right;
            //            isLeftChild = false;
            //        }
            //    }

            //    if (currentNode == null) 
            //    {
            //        Console.WriteLine("Could not find requested book. Removed nothing.");
            //        return; 
            //    }

            //    if (currentNode.Left == null && currentNode.Right == null)
            //    {
            //        if (isLeftChild)
            //        {
            //            parentNode.Left = null;
            //        }
            //        else
            //        {
            //            parentNode.Right = null;
            //        }
            //    }
            //    else if (currentNode.Left == null)
            //    {
            //        if (isLeftChild)
            //        {
            //            parentNode.Left = currentNode.Right;
            //        }
            //        else
            //        {
            //            parentNode.Right = currentNode.Right;
            //        }
            //    }
            //    else if (currentNode.Right == null)
            //    {
            //        if (isLeftChild)
            //        {
            //            parentNode.Left = currentNode.Left;
            //        }
            //        else
            //        {
            //            parentNode.Right = currentNode.Left;
            //        }
            //    }
            //    else
            //    {
            //        Node successor = GetSuccessor(currentNode);
            //        if (isLeftChild)
            //        {
            //            parentNode.Left = successor;
            //        }
            //        else
            //        {
            //            parentNode.Right = successor;
            //        }
            //    }
            //}

            //private Node GetSuccessor(Node node)
            //{
            //    Node successor = node;
            //    Node parentSuccessor = node;
            //    Node currentNode = node.Right;

            //    while (currentNode != null) 
            //    {
            //        parentSuccessor = successor;
            //        successor = currentNode;
            //        currentNode = currentNode.Left;
            //    }

            //    if (successor != node.Right)
            //    {
            //        parentSuccessor.Left = successor.Right;
            //        successor.Right = node.Right;
            //    }

            //    successor.Left = node.Left;

            //    return successor;
            //}

            private void InOrderTraversalRecursive(Node currentNode)
            {
                if (currentNode.Left != null)
                {
                    InOrderTraversalRecursive(currentNode.Left);
                }

                Console.WriteLine(currentNode.Key);

                if (currentNode.Right != null)
                {
                    InOrderTraversalRecursive(currentNode.Right);
                }
            }
        }

        static void Main(string[] args)
        {
            BinaryTree bt = new BinaryTree();

            List<Book> books = new List<Book>();

            Book book1 = new Book(15, "asdasd", "asdasd", 1992, 10);
            Book book2 = new Book(2, "asdasd", "asdasd", 1992, 10);
            Book book3 = new Book(3, "asdasd", "asdasd", 1992, 3);
            Book book4 = new Book(8, "asdasd", "asdasd", 1992, 10);
            Book book5 = new Book(5, "asdasd", "asdasd", 1992, 10);
            Book book6 = new Book(12, "asdasd", "asdasd", 1992, 10);
            Book book7 = new Book(13, "asdasd", "asdasd", 1992, 10);
            Book book8 = new Book(10, "asdasd", "asdasd", 1992, 10);
            Book book9 = new Book(11, "asdasd", "asdasd", 1992, 10);
            Book book10 = new Book(9, "asdasd", "asdasd", 1992, 10);

            bt.Insert(book1);
            bt.Insert(book2);
            bt.Insert(book3);
            bt.Insert(book4);
            bt.Insert(book5);
            bt.Insert(book6);
            bt.Insert(book7);
            bt.Insert(book8);
            bt.Insert(book9);
            bt.Insert(book10);

            Console.WriteLine();

            bt.InOrderTraversal();
            bt.RemoveByKey(8);
            bt.InOrderTraversal();


            Console.ReadLine();
        }
    }
}
