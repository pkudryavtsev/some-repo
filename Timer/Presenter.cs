﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimerMVP
{
    class Presenter
    {
        Model timer = null;

        MainWindow mainWindow = null;

        public Presenter(MainWindow mainWindow)
        {
            timer = new Model();
            this.mainWindow = mainWindow;
            this.mainWindow.startTimerEvent += StartTimer;
        }

        void StartTimer(object sender, EventArgs e)
        {
            
        }

        void StopTimer(object sender, EventArgs e)
        {

        }
    }
}
