﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Generics_Co_Contra
{
    public static class Extension
    {
        public static T[] GetArray<T>(this MyList<T> list)
        {
            return list.ToArray();
        }

        public static T[] Slice<T>(this T[] sourceArray, int startIndex, int endIndex)
        {
            if (endIndex < 0)
            {
                endIndex = sourceArray.Length + endIndex;
            }
            int length = endIndex - startIndex;

            T[] result = new T[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = sourceArray[i + startIndex];
            }
            return result;
        }
    }

    public class KeyValuePair <TKey, TValue>
    {
        public TKey Key { get; set; }
        public TValue Value { get; set; }

        public KeyValuePair(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }
    }

    public class MyDictionary <TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>
    {
        private KeyValuePair<TKey, TValue>[] dictionary;

        public int Count { get; private set; } = 0;

        public TValue this[TKey key]
        {
            get
            {
                return FindByKey(key);
            }
        }

        public MyDictionary()
        {
            dictionary = new KeyValuePair<TKey, TValue>[0];
        }

        public void AddPair(TKey key, TValue value)
        {
            if (CheckKey(key)) throw new KeyAlreadyExistsException(); 
            if (!CheckCellAvailability())
            {
                ExtendArrayLength(0);
            }
            dictionary[Count] = new KeyValuePair<TKey, TValue>(key, value);
            Count++;
        }

        private bool CheckCellAvailability()
        {
            return dictionary.Length > Count;
        }

        private int DefineArrayLength()
        {
            if (dictionary.Length == 0) return 2;
            else return dictionary.Length * 2;
        }

        private void ExtendArrayLength(int index)
        { 
            var tmpDictionary = new KeyValuePair<TKey, TValue>[DefineArrayLength()];
            dictionary.CopyTo(tmpDictionary, index);
            dictionary = tmpDictionary;
        }

        public void RemovePairByIndex(int index)
        {   
            for (int i = index; i < Count - 1; i++)
            {
                dictionary[i] = dictionary[i + 1];
            }
            Count--;
        }

        private TValue FindByKey(TKey key)
        {
            foreach (var pair in this)
            {
                if (pair.Key.Equals(key))
                {
                    return pair.Value;
                }

            }
            throw new KeyNotFoundException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns>True, если пара с таким ключом существует</returns>
        private bool CheckKey(TKey key)
        {
            for (int i = 0; i < Count; i++)
            {
                if (dictionary[i].Key.Equals(key))
                {
                    return true;
                }
            }
            return false;
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            for (int i = 0; i < Count; i++)
            {
                yield return dictionary[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return dictionary.GetEnumerator();
        }

        private class KeyAlreadyExistsException : Exception
        {
            public override string Message { get => "The key already exists in the dictionary."; }

            public KeyAlreadyExistsException()
            {

            }
        }
    }

    public class MyList <T> : IEnumerable<T>
    {
        private T[] array;

        public int Count { get; private set; } = 0; 

        public T this[int index]
        {
            get
            {
                return index > Count - 1 ? throw new IndexOutOfRangeException() : array[index];
            }
            set
            {
                array[index] = index > Count - 1 ? throw new IndexOutOfRangeException() : value;
            }
        }

        public MyList(int length = 1)
        {
            array = new T[length];
        }

        public IEnumerable<T> FindAll(Predicate<T> predicate)
        {
            return FindAll(predicate);
        }

        public IEnumerable<T> FindAll(Func<T, bool> predicate)
        {
            List<T> temp = new List<T>();

            for (int i = 0; i < array.Length; i++)
            {
                var tempItem = array[i];
                if (predicate.Invoke(tempItem))
                {
                    temp.Add(tempItem);
                }
            }
            return temp.ToArray();
        }

        public void AddFirst(T obj)
        {
            if (!CheckCellAvailability())
            {
                ExtendArrayLength(1);
            }
            else
            {
                for (int i = array.Length - 1; i >= 1; i--)
                    array[i] = array[i - 1];
                //if (array.Length > 1) array.CopyTo(array, 1);
            }
            array[0] = obj;
            Count++;
        }

        public void AddLast(T obj)
        {
            if (!CheckCellAvailability())
            {
                ExtendArrayLength(0);
            }
            array[Count] = obj;
            Count++;
        }

        public void RemoveByIndex(int index)
        {
            array = array.Take(index).Concat(array.Skip(index + 1)).ToArray();
            Count--;
        }

        private bool CheckCellAvailability()
        {
            return array.Length != Count;
        }

        private int DefineArrayLength()
        {
            if (array.Length == 0) return 2;
            return array.Length * 2;
        }

        private void ExtendArrayLength(int index)
        {
            T[] tmp = array.Take(Count).ToArray();
            array = new T[DefineArrayLength()];
            tmp.CopyTo(array, index);
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (T item in array)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        ~MyList()
        {
            Console.WriteLine("Object was destroyed");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            MyDictionary<int, string> dictionary = new MyDictionary<int, string>();
            dictionary.AddPair(1, "Hello World");
            dictionary.AddPair(2, "Hello Wo");
            dictionary.AddPair(3, "Hello Word");

            foreach (var pair in dictionary)
            {
                Console.WriteLine(pair.Key + ": " + pair.Value);
            }


            //Console.WriteLine(dictionary[1]);

            MyList<int> list = new MyList<int>();

            list.AddFirst(1);
            list.AddFirst(2);
            list.AddFirst(3);
            list.AddFirst(5);
            list.AddFirst(6);
            list.AddFirst(7);
            list.AddFirst(9);
            list.AddLast(10);
            list.AddLast(11);
            list.AddLast(12);
            list.AddLast(13);
            list.AddFirst(20);
            list.AddFirst(30);
            list[666] = 1;

            //List<Tuple<int, string>> testList = new List<Tuple<int, string>> 
            //{
            //    new Tuple<int, string>(5, "Не пизди на меня"), 
            //    new Tuple<int, string>(6, "Хуй пизда"), 
            //    new Tuple<int, string>(7, "Жопа") 
            //};

            //var a2 = testList.Where(x => x.Item1 == 5 || x.Item1 == 6).ToList();
            //var a3 = testList.Where(x => true).Select(x => new { number = x.Item1, text = x.Item2, text2 = (x.Item2 + " нахуй") }).OrderByDescending(x => x.number);

            //MyList<int> list = new MyList<int>();
            //list.AddFirst(1);
            //list.AddFirst(2);
            //list.AddFirst(3);
            //list.AddFirst(4);
            //list.AddFirst(5);

            //var a1 = list.Count;
            //var b1 = list.Count();

            //list.RemoveByIndex(2);

            //var a = list.FindAll(x => x > 3);

            //foreach (var item in a)
            //{
            //    Console.WriteLine(item.ToString());
            //}

            //Console.ReadLine();

            //string test = "Fuck123";
            //test = test.Replace("Fuck", "Peace");


        }
    }
}
