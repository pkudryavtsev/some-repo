﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWConsoleApp1_2
{
    class Program2
    {
        static void Main(string[] args)
        {
            int intInput1;
            Console.WriteLine("Please enter your number:");
            string input1 = Console.ReadLine();
            if (!Int32.TryParse(input1, out intInput1))
            {
                while (!Int32.TryParse(input1, out intInput1))
                {
                    Console.WriteLine("You entry is wrong, please enter an integer");
                    Console.WriteLine("Please enter your number");
                    input1 = Console.ReadLine();
                }
            }


            //string resultString;
            string result = (intInput1 > 5) && (intInput1 < 10) ? "Number is between 5 and 10" : "unknown number";

            Console.WriteLine(result);
            }
        }
    }
}
