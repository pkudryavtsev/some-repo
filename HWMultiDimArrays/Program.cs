﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace HWMultiDimArrays
{
    class MatrixClass
    {
        public int[,] Matrix { get; }

        public int this[int indexRow, int indexColumn]
        {
            get
            {
                return Matrix[indexRow, indexColumn];
            }
            set
            {
                Matrix[indexRow, indexColumn] = value;
            }
        }

        public int[,] E { get; }

        public int[,] O { get; }

        public MatrixClass(int rows, int columns)
        {
            Matrix = new int[rows, columns];
            E = CreateUnitMatrix(rows, columns);
            O = CreateZeroMatrix(rows, columns);
        }

        public int[,] CreateUnitMatrix(int rows, int columns)
        {
            int[,] matrix = new int[rows,columns];

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (i == j) matrix[i, j] = 1;
                }
            }

            return matrix;
        }

        public int[,] CreateZeroMatrix(int rows, int columns)
        {
            return new int[rows, columns];
        }


    }

    class Program
    {
        static void ShowMatrixByRow(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        static void ShowMatrixByRow(double[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        static void Main(string[] args)
        {
            double[,] userMatrix = new double[3, 3]
            {
                { 1, 3, 2 },
                { 2, 1, 5 },
                { 1, 0, 3 }
            };

            int[,] sourceMatrix = new int[3, 3]
            {
                { 0, 1, 3 },
                { 2, 3, 2 },
                { 3, 1, 1 }
            };

            Console.Read();
        }

        static int[,] MultiplyUnitAndUserMatrix1(int[,] userMatrix)
        {
            MatrixClass matrix = new MatrixClass(userMatrix.GetLength(0), userMatrix.GetLength(1));

            var unitMatrix = matrix.E;

            int[,] result = new int[userMatrix.GetLength(0), userMatrix.GetLength(1)];

            for (int i = 0; i < userMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < userMatrix.GetLength(1); j++)
                {
                    for (int k = 0; k < userMatrix.GetLength(1); k++)
                    {
                        result[i, j] += unitMatrix[i, k] * userMatrix[k, j];
                    }
                }
            }

            return result;
        }

        static int[,] ExtractFromNonDiagonalToMatrix2(int[,] sourceMatrix, int[,] destinationMatrix)
        {
            MatrixClass matrix = new MatrixClass(destinationMatrix.GetLength(0), destinationMatrix.GetLength(1));

            var placeholderMatrix = matrix.O;

            for (int i = 0; i < sourceMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < sourceMatrix.GetLength(1); j++)
                {
                    if (i != j)
                    {
                        placeholderMatrix[i, j] = sourceMatrix[i, j];
                        sourceMatrix[i, j] = -1;
                    }
                }
            }

            for (int i = 0; i < destinationMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < destinationMatrix.GetLength(1); j++)
                {
                    if (i != j) destinationMatrix[i, j] = placeholderMatrix[i, j];
                }
            }

            return destinationMatrix;
        }

        static int[,] PlaceMinInRowInFirstCol6(int[,] userMatrix)
        {
            for (int i = 0; i < userMatrix.GetLength(0); i++)
            {
                int currentMinIndex = 0;
                int currentMin = userMatrix[i, currentMinIndex];

                for (int j = 0; j < userMatrix.GetLength(1); j++)
                {
                    if (currentMin > userMatrix[i, j])
                    {
                        currentMin = userMatrix[i, j];
                        currentMinIndex = j;
                    }
                }

                if (currentMinIndex != 0)
                {
                    var temp = userMatrix[i, 0];
                    userMatrix[i, 0] = userMatrix[i, currentMinIndex];
                    userMatrix[i, currentMinIndex] = temp;
                }
            }

            return userMatrix;
        }

        static int[,] ExtractFromDiagonalToMatrix7(int[,] sourceMatrix, int[,] destinationMatrix)
        {
            MatrixClass matrix = new MatrixClass(destinationMatrix.GetLength(0), destinationMatrix.GetLength(1));

            var placeholderMatrix = matrix.O;

            for (int i = 0; i < sourceMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < sourceMatrix.GetLength(1); j++)
                {
                    if (i == j)
                    {
                        placeholderMatrix[i, j] = sourceMatrix[i, j];
                        sourceMatrix[i, j] = 0;
                    }
                }
            }

            for (int i = 0; i < destinationMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < destinationMatrix.GetLength(1); j++)
                {
                    if (i == j) destinationMatrix[i, j] = placeholderMatrix[i, j];
                }
            }

            return destinationMatrix;
        }
        
        static List<int> FindUniqueValues10(int[,] userMatrix)
        {
            List<int> result = new List<int>();

            var dict = new Dictionary<int, int>();

            for (int i = 0; i < userMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < userMatrix.GetLength(1); j++)
                {
                    if (!dict.ContainsKey(userMatrix[i, j]))
                    {
                        dict.Add(userMatrix[i, j], 1);
                    }
                    else
                    {
                        dict[userMatrix[i, j]]++;
                    }
                }
            }

            foreach(var pair in dict)
            {
                result.Add(pair.Key);
            }

            return result;
        }

        static int[,] TransposeMatrix12(int[,] userMatrix)
        {
            MatrixClass matrix = new MatrixClass(userMatrix.GetLength(1), userMatrix.GetLength(0));

            var resultMatrix = matrix.O;

            for (int i = 0; i < userMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < userMatrix.GetLength(1); j++)
                {
                    resultMatrix[j, i] = userMatrix[i, j];
                }
            }

            return resultMatrix;
        }


        // TODO
        static int[,] FillMatrixByCondition26(int n)
        {
            MatrixClass matrix = new MatrixClass(n, n);

            var result = matrix.O;

            for (int i = 0; i < result.GetLength(0); i++)
            {
                for (int j = 1 - i; j < result.GetLength(1) + 1; j++)
                {
                    if (j < 0)
                    {
                        result[i, j- 1 + i] = 0;
                    }
                    else
                    {
                        result[i, j-1 + i] = j;
                    }
                }
            }

            return result;
        }

        static double[,] SortDescendingAndChangeElementOnEvenIndex27(double[,] userMatrix)
        {
            double[] arrayToSort = new double[userMatrix.Length];

            for (int i = 0; i < userMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < userMatrix.GetLength(1); j++)
                {
                    arrayToSort[j + i * userMatrix.GetLength(1)] = userMatrix[i, j];
                }
            }

            Array.Sort(arrayToSort);
            var sortedArray = arrayToSort.Reverse().ToArray();

            for (int i = 0; i < userMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < userMatrix.GetLength(1); j++)
                {
                    userMatrix[i, j] = sortedArray[j + i * userMatrix.GetLength(1)];
                    if (((j % 2) == 0) && userMatrix[i, j] != 0) userMatrix[i, j] = 1 / userMatrix[i, j];
                }
            }


            return userMatrix;
        }

    }
}
