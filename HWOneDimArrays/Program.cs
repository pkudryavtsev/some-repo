﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace HWOneDimArrays
{
    class DynamicArray
    {
        public int[] Array;

        public int this[int index]
        {
            get
            {
                return Array[index];
            }
            set
            {
                Array[index] = value;
            }
        }

        public DynamicArray(int length = 0)
        {
            Array = new int[length];
        }
        
        public int GetLength()
        {
            return Array.Length;
        }

        public void Fill()
        {
            Random rand = new Random();
            for (int i = 0; i < Array.Length; i++)
            {
                Array[i] = rand.Next(10);
            }
        }

        public int FirstIndexOf(int x, int startIndex = 0)
        {
            for (int i = startIndex; i < Array.Length; i++)
            {
                if (Array[i] == x) return startIndex;
            }
            return -1;
        }

        public int LastIndexOf(int x, int startIndex = 0)
        {
            for (int i = Array.Length - 1; i >= startIndex; i--)
            {
                if (Array[i] == x) return startIndex;
            }
            return -1;
        }

        public void AddFirst(int x)
        {
            int[] temp = Array;
            Array = new int[Array.Length + 1];
            for (int i = 1; i < Array.Length; i++)
            {
                Array[i] = temp[i - 1];
            }
            Array[0] = x;
        }

        public void AddLast(int x)
        {
            int[] temp = Array;
            Array = new int[Array.Length + 1];
            for (int i = 0; i < Array.Length - 1; i++)
            {
                Array[i] = temp[i];
            }
            Array[Array.Length - 1] = x;

        }

        public void RemoveByIndex(int index)
        {
            int[] temp = new int[Array.Length - 1];

            for (int i = 0; i < index; i++)
            {
                temp[i] = Array[i];
            }

            for (int j = index + 1; j < Array.Length; j++)
            {
                temp[j - 1] = Array[j];
            }

            Array = temp;
        }

        public void ReversePartial(int startIndex, int length)
        {
            int[] temp = new int[length];

            for (int i = 0; i < temp.Length; i++)
            {
                temp[i] = Array[startIndex + i];
            }

            int j = length - 1;
            for (int i = 0; i < length / 2; i++)
            {
                var tempValue = temp[i];
                temp[i] = temp[j];
                temp[j] = tempValue;
                j--;
            }

            for (int i = 0; i < length; i++)
            {
                Array[startIndex + i] = temp[i];
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        ~DynamicArray()
        {
            Console.WriteLine("Object was destroyed");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            DynamicArray dynamicArray = new DynamicArray();

            dynamicArray.AddFirst(3);
            dynamicArray.AddFirst(1);
            dynamicArray.AddFirst(2);
            dynamicArray.AddFirst(5);
            dynamicArray.AddFirst(4);
            dynamicArray.AddFirst(7);
            dynamicArray.AddFirst(2);

            dynamicArray.ReversePartial(0, 4);


            int[] A = { 2, 6, -1, -4, 4, 7, -10, 5, -2, 4 };
            //          0  1  2  3  4  5  6   7  8  9

            int[] Task26 = {5, -1, 6, 2, -4, 7, 3, -2, 3};

            var arr = DivideBySide3UsingOwnClass(Task26);

            int[] B = { 3, 0, 1, 2, 0, 6, 2, 0, 4 };

            double[] Pi = { Math.PI / 4, Math.PI / 6, (7 * Math.PI) / 4, (3 * Math.PI) / 2 };

            var a = CalculateValueWithMaxNegativeSin22(Pi);

            var b = CalculateSumBetweenFirstAndLastZeros14(B);


            Console.ReadKey();
        }

        
        public static List<int> CalculateDifferentValues1(int[] array)
        {
            List<int> result = new List<int>();
            var dict = new Dictionary<int, int>();

            foreach (int n in array)
            {
                if (dict.ContainsKey(n))
                {
                    dict[n]++;
                }
                else
                {
                    dict[n] = 1;
                }
            }
            
            foreach(var pair in dict)
            {
                result.Add(pair.Key);
            }

            return result;
        }

        static int CalculateSumOfFibonacciIndex2(int[] array)
        {
            int sum = 0;
            int[] fibArray = PopulateWithFibonacci4(array.Length);

            for (int i = 0; i < array.Length; i++)
            {
                if(fibArray.Contains(i))
                {
                    sum += array[i];
                }
            }
            return sum;
        }

        static int[] DivideBySide3(int[] array)
        {
            var positives = new List<int>();
            var zeros = new List<int>();
            var negatives = new List<int>();

            for (int i = 0; i < array.Length; i++)
            {
                var currentEl = array[i];
                if (currentEl > 0) positives.Add(currentEl);
                if (currentEl == 0) zeros.Add(currentEl);
                if (currentEl < 0) negatives.Add(currentEl);
            }

            int[] result = new int[array.Length];

            for (int i = 0; i < positives.Count; i++)
            {
                result[i] = positives[i];
            }

            for (int i = positives.Count; i < (zeros.Count + positives.Count); i++)
            {
                result[i] = zeros[i - positives.Count];
            }

            for (int i = (zeros.Count + positives.Count); i < (zeros.Count + positives.Count + negatives.Count); i++)
            {
                result[i] = negatives[i - positives.Count - zeros.Count];
            }

            return result;
        }

        static int[] DivideBySide3Alternative(int[] array)
        {
            LinkedList<int> result = new LinkedList<int>();

            List<int> positiveList = new List<int>();

            for (int i = 0; i < array.Length; i++)
            {
                var currentEl = array[i];
                if (currentEl < 0) result.AddLast(currentEl);
                if (currentEl > 0) positiveList.Add(currentEl);
            }

            positiveList.Reverse();

            for (int i = 0; i < positiveList.Count; i++)
            {
                result.AddFirst(positiveList[i]);
            }

            return result.ToArray();
        }

        static DynamicArray DivideBySide3UsingOwnClass(int[] array)
        {
            DynamicArray result = new DynamicArray();

            int counterNegatives = 0;
            for (int i = 0; i < array.Length; i++)
            {
                var currentEl = array[i];
                if (currentEl < 0) { result.AddLast(currentEl); counterNegatives++; }
                if (currentEl > 0) result.AddFirst(currentEl);
            }

            result.ReversePartial(0, result.GetLength() - counterNegatives);

            return result;
        }

        static int[] PopulateWithFibonacci4(int length)
        {
            int[] result = new int[length];

            for (int i = 0; i < result.Length; i++)
            {
                if (i == 0) result[i] = 0;
                else if (i == 1 || i == 2) result[i] = 1;
                else result[i] = result[i - 1] + result[i - 2];
            }

            return result;
        }

        static int CalculateZeros5And16(int[] array)
        {
            var a = array.Count(x => x == 0);

            int counter = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 0) counter++;
            }
            return counter;
        }

        static int CalculateOccurenceOfValueMax6(int[] array)
        {
            List<int> listOfOccurences = new List<int>();
            var dict = new Dictionary<int, int>();

            foreach (int n in array)
            {
                if (dict.ContainsKey(n))
                {
                    dict[n]++;
                }
                else
                {
                    dict[n] = 1;
                }
            }

           foreach(var pair in dict)
            {
                if (pair.Value > 1) listOfOccurences.Add(pair.Value);
            }

            return listOfOccurences.Max();
        }

        static (int, int) FindIndexOfMinAndMax7(int[] array)
        {
            if (array.Length <= 0)
            {
                throw new Exception("Cannot perform operation on empty array");
            }

            int min = array[0];
            int minIndex = 0;
            int max = array[0];
            int maxIndex = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (min > array[i])
                {
                    min = array[i];
                    minIndex = i;
                }
                if (max < array[i])
                {
                    max = array[i];
                    maxIndex = i;
                }
            }

            return (minIndex, maxIndex);
        }

        static List<int> CalculateSortedOddNumbers8(int[] array)
        {
            List<int> result = new List<int>();

            for (int i = 0; i < array.Length; i++)
            {
                var temp = array[i];
                if ((temp % 2) != 0) result.Add(temp);
            }
            result.Sort();

            return result;
        }

        static int CalculateSumOfPositiveNumbers9(int[] array)
        {
            int sum = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > 0) sum += array[i];
            }

            return sum;
        }

        static int CalculateSumOfPositiveNumbers9LINQ(int[] array)
        {
            return array.Sum(x => x > 0 ? x : 0);
        }

        static int CalculateDifferenceMaxMinIndex10And27(int[] array)
        {
            int result;
            int min = array[0];
            int minIndex = 0;
            int max = array[0];
            int maxIndex = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (min > array[i]) 
                { 
                    min = array[i];
                    minIndex = i;
                }
                if (max < array[i])
                {
                    max = array[i];
                    maxIndex = i;
                }
            }

            result = Math.Abs(maxIndex - minIndex);

            return result;
        }

        static int CalculateSinBiggerThanCos11(int[] array)
        {
            int counter = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (Math.Sin(array[i]) > Math.Cos(array[i]))
                {
                    counter++;
                }
            }
            return counter;
        }

        static int CalculateZerosAtOddIndex12(int[] array)
        {
            int counter = 0;
            for (int i = 0; i < array.Length; i+=2)
            {
                if ((array[i] == 0)) counter++;
            }
            return counter;
        }

        static List<int> CalculateReverseSortedOddNumbers13(int[] array)
        {
            List<int> result = new List<int>();

            for (int i = 0; i < array.Length; i++)
            {
                var temp = array[i];
                if ((temp % 2) != 0) result.Add(temp);
            }

            result.Sort();
            result.Reverse();

            return result;
        }

        static int CalculateSumBetweenFirstAndLastZeros14(int[] array)
        {
            int sum = 0;

            if (array.Length <= 0) throw new Exception("Cannot perform calculation on empy array");

            int indexFirst = -1;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 0) { indexFirst = i; break; }
            }

            int indexLast = -1;
            for (int i = array.Length - 1; i >= 0; i--)
            {
                if (array[i] == 0) { indexLast = i; break; }
            }

            if (indexFirst == indexLast)
            {
                return 0;
            }

            for (int i = indexFirst + 1; i < indexLast; i++)
            {
                sum += array[i];
            }

            return sum;
        }

        static void SortValuesAtEvenIndexBySquaredValue15(int[] array)
        {
            int[] result = new int[array.Length];

            List<int> valuesAtEvenIndex = new List<int>();

            for (int i = 0; i < array.Length; i+=2)
            {
                valuesAtEvenIndex.Add(array[i]);
            }

            var sortedValuesBySquare = valuesAtEvenIndex.OrderBy(x => x * x).ToList();

            for (int i = 0; i < array.Length; i += 2)
            {
                array[i] = sortedValuesBySquare[i/2];
            }
        }

        static int CalculateSumOfEvenValuesAtOddIndex17(int[] array)
        {
            int sum = 0;

            for (int i = 0; i < array.Length; i+=2)
            {
                if ((array[i] % 2) == 0) sum += array[i];
            }

            return sum;
        }

        static double CalculateArithmeticMean18(int[] array)
        {
            double result = 0;
            int sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sum += array[i];
            }
            result = (double) sum / array.Length;

            return result;
        }

        static double CalculateArithmeticMean18LINQ(int[] array)
        {
            return (double)array.Sum(x =>  x) / array.Length;
        }

        static double CalculateCosValuesWithNegativeSin19(int[] array)
        {
            double sum = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (Math.Sin(array[i]) < 0) sum += Math.Cos(array[i]);
            }

            return sum;
        }

        static double CalculateCosValuesWithNegativeSin19LINQ(int[] array)
        {
            return array.Sum(x => (Math.Sin(x) < 0) ? Math.Cos(x) : 0);
        }

        static int[] CalculateIndexOfSortedValuesInDefaultArray20(int[] array)
        {
            int[] defaultIndexesOfArray = new int[array.Length];
            for (int i = 0; i < defaultIndexesOfArray.Length; i++)
            {
                defaultIndexesOfArray[i] = i;
            }


            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length - 1 - i; j++)
                {
                    if (array[j] > array[j+1])
                    {
                        var temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;

                        var tempIndex = defaultIndexesOfArray[j];
                        defaultIndexesOfArray[j] = defaultIndexesOfArray[j + 1];
                        defaultIndexesOfArray[j + 1] = tempIndex;
                    }
                }
            }

            foreach (var item in defaultIndexesOfArray)
            {
                Console.Write(item + " ");
            }

            return defaultIndexesOfArray;
        }

        
        static int[] CalculateThreeMaxValues21(int[] array)
        {
            int[] result = new int[3];

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < array.Length - i - 1; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        var temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
            }

            for (int counter = 0; counter <= 2; counter++)
            {
                result[counter] = array[array.Length - 1 - counter];
            }

            return result;
        }

        //TODO Сделать вторым способом
        static int[] CalculateThreeMaxValues21Optional(int[] array)
        {
            int[] result = { array[0], array[1], array[2] };

            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < result.Length; j++)
                {
                    if (array[i] > result[j]) result[j] = array[i];
                }
            }

            return result;
        }

        static double CalculateValueWithMaxNegativeSin22(double[] array)
        {
            double maxNum = array[0];
            double minSin = Math.Sin(maxNum);

            for (int i = 0; i < array.Length; i++)
            {
                var currentEl = array[i];
                if ((Math.Sin(currentEl) < 0) && (minSin > Math.Sin(currentEl)))
                {
                    minSin = Math.Sin(currentEl);
                    maxNum = currentEl;
                }
            }

            return minSin > 0 ? -1 : maxNum;
        }

        static int CalculateIndexOfMaxValue23(int[] array)
        {
            int max = array[0];
            int maxIndex = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (max < array[i]) 
                { 
                    max = array[i];
                    maxIndex = i;
                }
            }

            return maxIndex;
        }

        static int CalculateIndexOfMaxValue23LINQ(int[] array)
        {
            return Array.IndexOf(array, array.Max());
        }

        static void SortBySin24(int[] array)
        {
            for (int i = 0; i < array.Length - 1; i++)
            {
                for (int j = 0; j < array.Length - 1 - i; j++)
                {
                    if (Math.Sin(array[j]) > Math.Sin(array[j + 1]))
                    {
                        var temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
            }
        }

        static int[] SortBySin24LINQ(int[] array)
        {
            List<int> result = new List<int>(array.ToList());
            result.Sort((x, y) => Math.Sin(x).CompareTo(Math.Sin(y)));
            return result.ToArray();
        }

        static int[] PopulateWithFactValues25(int n)
        {
            if (n > 10)
            {
                throw new Exception("Your entered number was too high");
            }

            int[] result = new int[n];

            for (int i = 0; i < result.Length; i++)
            {
                result[i] = Factorial(i);
            }

            return result;
        }

        static int Factorial(int n)
        {
            return (n != 1) && (n != 0)? n * Factorial(n - 1) : 1;
        }

        static int CalculateSumByCondition26(int[] array)
        { 
            List<int> sumList = new List<int>();

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > 0) 
                { 
                    sumList.Add(array[i]);
                    i += sumList.Count - 1;
                }
                else if (array[i] < 0) continue;
            }

            return sumList.Sum();
        }

        static int CountByCondition28(int[] array)
        {
            int counter = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (((double) array[i] / array[array.Length - 1 - i]) > 1) counter++;
            }

            return counter;
        }

        static int[] ModifyValueByCondition29(int[] array)
        {
            int[] result = new int[array.Length];

            for (int i = 0; i < array.Length; i++)
            {
                result[i] = array[i] * array[array.Length - 1 - i];
            }

            return result;
        }

        static double CalculateGeometricMean30(int[] array)
        {
            double result = 0;
            double product = 1;

            for (int i = 0; i < array.Length; i++)
            {
                product *= array[i];
            }
            result = Math.Pow(product, (double) 1 / array.Length);

            return result;
        }
        
    }
}
