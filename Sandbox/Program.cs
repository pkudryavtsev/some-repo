﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 5;
            object b = a;


           a = (int) b;



            int[][] romb = new int[][]
            {
                new int[] {1},
                new int[] { 5, 2 },
                new int[] {0, 4, 3},
                new int[] {2, 1, 5, 0},
                new int[] {3, 0, 4},
                new int[] { 5, 1},
                new int[] {6}
            };

            int numOfDims = (romb.Length + 1) / 2;

            int[,] matrixArr = new int[numOfDims, numOfDims];

            for (int angle = 0; angle < numOfDims; angle++)
            {
                for (int i = 0; i < numOfDims - angle; i++)
                {
                    if (i == numOfDims - angle - 1)
                    {
                        for (int j = 0; j < numOfDims - angle; j++)
                        {
                            matrixArr[i, j+angle] = romb[j+i+angle][angle];
                            Console.WriteLine($"Matrix at ({i}, {(angle == numOfDims - 1? angle : j)}) = " + romb[j+i+angle][angle]);
                        }
                        continue;
                    }
                    matrixArr[i, angle] = romb[i+angle][angle];
                    Console.WriteLine($"Matrix at ({i}, {angle}) = " + romb[i + angle][angle]);
                }
                Console.WriteLine();
            }

            for (int i = 0; i < matrixArr.GetLength(0); i++)
            {
                for (int j = 0; j < matrixArr.GetLength(1); j++)
                {
                    Console.Write(matrixArr[i,j] + " ");
                }
                Console.WriteLine();
            }

            //int[,] matrix = new int[,]
            //{
            //    { 1, 2, 3 },
            //    { 5, 4, 1},
            //    { 0, 5, 6 } 
            //};
            //int numOfDiagonals = matrix.GetLength(0) + matrix.GetLength(1) - 1;

            //int[][] arrayDiag = new int[numOfDiagonals][];

            //for (int n = numOfDiagonals; n > 0; n--)
            //{
            //    for(int i = 0, j = 0; (i < matrix.GetLength(0) && j < matrix.GetLength(1));)
            //    {
                    
            //    }
            //}

            Console.Read();
        }
    }
}
