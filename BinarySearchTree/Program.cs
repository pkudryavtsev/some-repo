﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.XPath;

namespace BinarySearchTree
{
    public class Book
    {
        // Properties
        public int ID { get; }
        public string Author { get; }
        public string Title { get; }
        public int YearOfPublishing { get; }
        public int Quantity { get; }

        // Constructor
        public Book(int id, string author, string title, int yearOfPublishing, int quantity)
        {
            ID = id;
            Author = author;
            Title = title;
            YearOfPublishing = yearOfPublishing;
            Quantity = quantity;
        }
    }

    public class Node
    {
        private Book data;

        public Book Data 
        { 
            get { return data; } 
        }

        private Node rightNode;

        public Node RightNode 
        { 
            get { return rightNode; }
            set { rightNode = value; }
        }

        private Node leftNode;

        public Node LeftNode 
        { 
            get { return leftNode; }
            set { leftNode = value; }
        }

        public Node(Book book)
        {
            data = book;
            rightNode = null;
            leftNode = null;
        }

        // [x]
        public void Insert(Book book)
        {
            if (book.ID >= Data.ID)
            {
                if (rightNode == null)
                {
                    rightNode = new Node(book);
                }
                else
                {
                    rightNode.Insert(book);
                }
            }
            else
            {
                if (leftNode == null)
                {
                    leftNode = new Node(book);
                }
                else
                {
                    leftNode.Insert(book);
                }
            }
        }

        // [x]
        public Node FindById(int id)
        {
            Node currentNode = this;

            while (currentNode != null)
            {
                if (id == currentNode.Data.ID)
                {
                    return currentNode;
                }
                else if (id > currentNode.Data.ID)
                {
                    currentNode = currentNode.rightNode;
                }
                else
                {
                    currentNode = currentNode.leftNode;
                }
            }

            return null;
        }

        // [x]
        public void InOrderTraversal()
        {
            if (leftNode != null)
            {
                leftNode.InOrderTraversal();
            }

            // Output of "current" root data
            Console.WriteLine(data.ID + " ");

            if (rightNode != null)
            {
                rightNode.InOrderTraversal();
            }
        }
    }

    public class BinaryTree
    {
        private Node root;

        // [x]
        public void Insert(Book book)
        {
            if (root != null)
            {
                root.Insert(book);
            }
            else
            {
                root = new Node(book);
            }
        }

        // Not sure what to do with two children
        // [x]
        public void RemoveById(int id)
        {
            Node currentNode = root;
            Node parentNode = root;
            bool isLeftChild = false;

            if (currentNode == null)
            {
                return;
            }

            while (currentNode != null && currentNode.Data.ID != id)
            {
                parentNode = currentNode;

                if (id < currentNode.Data.ID)
                {
                    currentNode = currentNode.LeftNode;
                    isLeftChild = true;
                }
                else
                {
                    currentNode = currentNode.RightNode;
                    isLeftChild = false;
                }
            }

            if (currentNode == null)
            {
                return;
            }

            if (currentNode.LeftNode == null && currentNode.RightNode == null)
            {
                if (currentNode == root)
                {
                    root = null;
                }
                else
                {
                    if (isLeftChild)
                    {
                        parentNode.LeftNode = null;
                    }
                    else
                    {
                        parentNode.RightNode = null;
                    }
                }
            }
            else if (currentNode.RightNode == null)
            {
                if (currentNode == root)
                {
                    root = null;
                }
                else
                {
                    if (isLeftChild)
                    {
                        parentNode.LeftNode = currentNode.LeftNode;
                    }
                    else
                    {
                        parentNode.RightNode = currentNode.LeftNode; 
                    }
                }
            }
            else if (currentNode.LeftNode == null)
            {
                if (currentNode == root)
                {
                    root = null;
                }
                else
                {
                    if (isLeftChild)
                    {
                        parentNode.LeftNode = currentNode.RightNode;
                    }
                    else
                    {
                        parentNode.RightNode = currentNode.RightNode;
                    }
                }
            }
            else
            {
                Node successor = GetSuccessor(currentNode);

                if (currentNode == root)
                {
                    root = successor;
                }
                else if (isLeftChild)
                {
                    parentNode.LeftNode = successor;
                }
                else
                {
                    parentNode.RightNode = successor;
                }
            }
        }

        private Node GetSuccessor(Node node)
        {
            Node parentOfsuccessor = node;
            Node successor = node;
            Node current = node.RightNode;

            while (current != null)
            {
                parentOfsuccessor = successor;
                successor = current;
                current = current.LeftNode;
            }

            if (successor != node.RightNode)
            {
                parentOfsuccessor.LeftNode = successor.RightNode;
                successor.RightNode = node.RightNode;
            }

            successor.LeftNode = node.LeftNode;

            return successor;
        }

        // Think about finding by attribute
        // [x]
        public Node FindById(int id)
        {
            if (root != null)
            {
                return root.FindById(id);
            }
            else
            {
                return null;
            }
        }

        public int ShowAvailability(Book book)
        {
            return -1;
        }


        // Think about traversing by attibute
        // [x]
        public void InOrderTraversalByAttribute()
        {
            //string traverseByAttribute = string.Empty;
            //switch (attribute.ToLower())
            //{
                
            //}

            if (root != null)
            {
                root.InOrderTraversal();
            }
        } 

    }

    class Program
    {
        static void Main(string[] args)
        {
            BinaryTree bst = new BinaryTree();

            List<Book> books = new List<Book>();

            Book book1 = new Book(15, "asdasd", "asdasd", 1992, 10);
            Book book2 = new Book(2, "asdasd", "asdasd", 1992, 10);
            Book book3 = new Book(3, "asdasd", "asdasd", 1992, 10);
            Book book4 = new Book(8, "asdasd", "asdasd", 1992, 10);
            Book book5 = new Book(5, "asdasd", "asdasd", 1992, 10);
            Book book6 = new Book(12, "asdasd", "asdasd", 1992, 10);
            Book book7 = new Book(13, "asdasd", "asdasd", 1992, 10);
            Book book8 = new Book(10, "asdasd", "asdasd", 1992, 10);
            Book book9 = new Book(11, "asdasd", "asdasd", 1992, 10);
            Book book10 = new Book(9, "asdasd", "asdasd", 1992, 10);

            books.Add(book1);
            books.Add(book2);
            books.Add(book3);
            books.Add(book4);
            books.Add(book5);
            books.Add(book6);
            books.Add(book7);
            books.Add(book8);
            books.Add(book9);
            books.Add(book10);


            for (int i = 0; i < books.Count; i++)
            {
                bst.Insert(books[i]);
            }


            bst.RemoveById(12);
            Console.ReadKey();

            bst.InOrderTraversalByAttribute();

            Console.ReadKey();
        }
    }
}
