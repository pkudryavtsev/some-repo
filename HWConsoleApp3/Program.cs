﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWConsoleApp3
{
    class Program
    {
        const double PI = 3.14;

        static void Main(string[] args)
        {
            Exercise1();
            Exercise2();
            Exercise3(4);
            Exercise4(4, 5);
            Exercise6(10, 12);

            Console.ReadKey();
        }

        public static void Exercise1()
        {
            int x = 10, y = 12, z = 3;
            //x += y - x++ * z;
            //x = y++ * z;
            Console.WriteLine(x += (y - ((x++) * z))); // -8
            Console.WriteLine(z = ((--x) - (y * 5))); //-69
            Console.WriteLine(y /= (x + (5 % z))); //-3
            Console.WriteLine(z = ((x++) + (y * 5))); //-23 = -8 + (-15); Why -24?
            Console.WriteLine(x = (y - ((x++) * z))); //-199 = -3 - 195; Why -195?
        }

        static void Exercise2()
        {
            int x = 10, y = 12, z = 3;
            var result = (x + y + z) / 3; // Result variable is Int displaying only full number. Even if assigning result as double, it converts to int. Requires casting
            Console.WriteLine(result);
        }

        static void Exercise3(int r)
        {
            var result = PI * r * r;
            Console.WriteLine(result);
        }

        static void Exercise4(int r, int h)
        {
            var resultV = PI * r * r * h;
            var resultS = 2 * PI * r * (r + h);
            Console.WriteLine(resultV);
            Console.WriteLine(resultS);
        }

        static void Exercise5()
        {
            //int uberflu? = 3;
            int _Identifier = 4;
            int \u006fIdentifier = 5;
            //int &myVar = 5;
            int myVariab1le = 6;

        }

        static void Exercise6(int x, int y)
        {
            x += y;
            y = x - y;
            x -= y;
            Console.WriteLine(x);
            Console.WriteLine(y);
        }
    }
}
