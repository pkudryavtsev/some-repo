﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWConsoleApp2
{
    class Program
    {
        enum OperationType
        {
            Add,
            Subtract,
            Multiply,
            Divide
        }
        static void Main(string[] args)
        {
            string[] ops = new string[] { "Add", "Subtract", "Multiply", "Divide" };

            Console.WriteLine("Please select your operation by index, sign or name (e.g. add):");

            foreach(string op in ops)
            {
                Console.WriteLine($"For {op} enter {Array.IndexOf(ops, op)}");
            }            

            string input = Console.ReadLine().ToLower();

            switch (input)
            {
                case "0":
                case "add":
                case "+":
                    Console.WriteLine($"You picked addition");
                    break;
                case "1":
                case "subtract":
                case "-":
                    Console.WriteLine($"You picked subtraction");
                    break;
                case "2":
                case "multiply":
                case "*":
                    Console.WriteLine($"You picked multiplication");
                    break;
                case "3":
                case "divide":
                case "/":
                    Console.WriteLine($"You picked division");
                    break;
                default:
                    Console.WriteLine("No such operation is known");
                    break;
            }

            /*
            int typedInput;
            bool condition = int.TryParse(input, out typedInput) && (typedInput >= 0) && (typedInput <= ops.Length - 1);
            if (condition)
            {
                foreach(string op in ops)
                {
                    op.ToLower();
                    if (typedInput == Array.IndexOf(ops, op))
                    {
                        Console.WriteLine($"You picked {op}");
                        break;
                    }
                }
            }
            else
            {
                Console.WriteLine("Please enter a correct index and try again");
            }
            */


            Console.Read();

        }               
    }
}
